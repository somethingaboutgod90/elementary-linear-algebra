GenerateProblems.ipynb

# 1.  Support Routines
## 1.1 LaTeX for Itikz
## 1.2 Matrix Generation Routines
## 1.3 Gaussian Elimination Routine
## 1.4 QR Routine
# 2. GE Problems
## 2.1 GE Problem with Layout
## 2.2 Inverse Problem with Layout
## 2.3 PLU Problem
# 3 Normal Equation Problems
## 3.1 Solve the Normal Equation
# 4. QR Problems
## 4.1 Orthogonal Matrices
## 4.2 QR
# 5. Eigenproblems
