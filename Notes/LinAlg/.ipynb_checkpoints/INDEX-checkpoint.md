---
tags: [#index]
---
# Matrix Operations
[[01 Scalars Vectors  Matrices]]
[[02 Addition Scalar Multiplication Dot product]]
[[03 Matrix Multiplication]]
[[04 Vector and Matrix Algebra]]

# The $A x = b$ Problem
## Gaussian Elimination and its Consequences
### 1. Gaussian Elimination
[[05 Rowechelon Form Systems]]
[[06 Gaussian Elimination - Elementary Operations]]
[[07 Gaussian Elimination Algorithm]]

### 2: Special Case Inverse, GE Reformulation
[[11 The Inverse of a Matrix]]
[[12 LU Decomposition]]

### 3: Homogeneous Solutions, Consequences, Generalization
[[08 Linear Independence]]
[[15 Vector Spaces]]
[[16 Basis fo a Vector Space]]

## Turn Around: Linear Transformations
[[09 Linear Transformations]]
[[10a Linear Transformations Examples]]
[[10b Linear Tansformation Examples]]
[[10c Linear Transformations with GeoGebra]]

## Length and its Consequences

[[20 Length and Orthogonality]]
[[21a Linear Transformations; Gram-Schmidt]]
[[22 QR Decomposition]]
[[23 Metric Spaces]]
[[13 The Wedge Product]]
[[14 Determinants]]
[[30 Vector and Matrix Norms]]

# The $A x = \lambda x$ Problem
## The Eigenproblem
[[17 Eigen Analysis]]
[[18 Eigen Computations]]
[[19 Diagonalization of Matrices]]

[[24 The Spectral Theorem]]
[[25 Positive Definite Matrices]]
[[26 The Gram Matrix]]
[[27 The Singular Value Decomposition (SVD)]]
[[28 The Pseudo Inverse]]
[[29 SVD Applications]]

[[Rayleigh Quotients]]

# Appendices, Miscellaneous
[[A1 Analytic Geometry]]
[[A2 Matrix Layout Displays]]

[[The Fourier Matrix]]
[[GE Layout Display]]
[[Generate Problems]]
[[The Hamilton  Cayley Theorem]]

[[Iterative Methods]]
[[Iterative Methods with Julia]]
[[Iterative Methods with Python]]

[[Regression]]
[[Randomized SVD]]
[[Matrix Multiplication Summary]]

[[other Geogebra]]
[[other Iteration Examples]]
[[other Least Mean Squares]]
[[delete]]


[[other Projections]]
[[other Three Bases Example]]