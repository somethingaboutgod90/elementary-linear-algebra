---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[A2 Matrix Layout Displays]]
---

A1_AnalyticGeometry.ipynb

# 1. Vectors
# 2. Formulae from Trigonometry
## 2.1 Euler's Formula
## 2.2 Formulae based on Euler's Formula and Complex Multiplication
# 3. Example
### 3.1 Reflection With Respect To a Line
## 3.2 Orthogonal Projection Onto a Line
# 4. Take Away
