---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
---

GE_layout_display.ipynb

# 1. Display a Layout of the Gaussian Elimination Algorithm
## 1.1 Basic Display Function: build a table from the matrices
## 1.2 Gaussian Elimination for a given matrix $A$
## 1.3 Modify ge_layout to highlight the pivots
## 1.4 Add a slider to animate the computation
# 2. Coding Assignment
## 2.1 Some code building blocks
### 2.1.1 Submatrices
### 2.1.2 Conditional Statements
### 2.1.3 Loop statements `for loops`
# 2.2 Needed functionality: code snippets you will need to write
# 3. Having fun: what if we have errors in A?
## 3.1 Errors in the matrix
## 3.2 Errors in the right hand side
## 3.3 Errors in both the matrix and the right hand side
## 3.4 What if the system is almost inconsistent
