---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
up: [[INDEX]]
tags:   []
---

A2_Matrix_Layout_Displays.ipynb

# 1. Gaussian Elimination and Gauss Jordan Elimination Examples
## 1.1 Stack of Matrices
## 1.2 Decorating a Matrix
## 1.3 Gaussian Elimination Example
## 1.4 Lower Code Level
# 2. QR Examples
## 2.1 Sympy Format Example
## 2.2 Floating Point Format Example
# 3. Eigenproblem Tables
## 3.1 Basic Eigenproblem Table
## 3.2 Spectral Theorem Table
## 3.3 SVD Table
