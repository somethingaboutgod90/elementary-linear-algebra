---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '01_ScalarsVectorsMatrices.ipynb'
status: 'complete'
tags:   ['operations', 'scalars', 'vectors', 'matrices']
up: [[INDEX]]
next: [[02 Addition Scalar Multiplication Dot product]]
---

File: [notebook](file:/home/lab/NOTEBOOKS/elementary-linear-algebra/notebooks/01_ScalarsVectorsMatrices.ipynb)
[new tab, same server](http://localhost:8888/lab/workspaces/auto-K/tree/NOTEBOOKS/file:///home/lab/NOTEBOOKS/elementary-linear-algebra/notebooks/01_ScalarsVectorsMatrices.ipynb)
<% tp.user.jupyterlink( tp.frontmatter.media.notebook, 8888) %>

# 1. Introduction
## 1.1 Scalars
## 1.2 Vectors and Matrices
# 2. Special Cases
## 2.1 Matrices With a Single Row or a Single Column
## 2.2 Zero Matrix, Identity Matrix
# 3. Geometrical Representation of Vectors
## 3.1 Arrows
## 3.2 Functions
# 4. Take Away

