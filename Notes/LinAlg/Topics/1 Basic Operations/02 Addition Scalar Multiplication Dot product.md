---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '02_AddScalarMultDotprod.ipynb'
status: 'complete'
tags:   []
up: [[INDEX]]
next: [[03 Matrix Multiplication]]
---

02_AddScalarMultDotprod.ipynb

# 1. Notation and Remarks
# 2. Equality, Addition and Scalar Multiplication
## 2.1 Equality
## 2.2 Addition
## 2.3 Scalar Multiplication
## 2.4 Subtraction
## 2.5 System of Equations Example
### 2.5.1 Important Example
### 2.5.2 Two <strong>Very Important</strong> Definitions
# 3. Properties
## 3.1 Algebraic Properties
## 3.2 Geometric Representation
### 3.2.1 Vector Addition and Subtraction
### 3.2.2 Linear Combinations of Vectors
# 4 Take Away
