---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[05 Rowechelon Form Systems]]
---

Summary_01_MatrixMultiplication.ipynb

# 1. Definition of Matrix Multiplication
## 1.1. The Dot Product
## 1.2 Generalization: The Matrix Product
# 2. Special Cases
## 2.1 Inner and outer products
### 2.1.1 Inner Product: Row Vector Times Column Vector
### 2.1.2 Outer Product: Column Vector times Row Vector
## 2.2 Matrix Times Column Vector
# 3. Submatrices
## 3.1 We can partition the $A$ and $C$ matrices horizontally
## 3.2 We can partition the $B$ and $C$ matrices vertically
## 3.3 We can partition the $A$ matrix vertically, and the $B$ matrix horizontally
## 3.4 We can partition further, horizontally and vertically...
# 4. Notation convention
# 5. Key Take Away
