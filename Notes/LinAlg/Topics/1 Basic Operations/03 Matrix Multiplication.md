---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '03_MatrixMultiplication.ipynb'
status: 'complete'
tags:   []
up: [[INDEX]]
next: [[04 Vector and Matrix Algebra]]
---

[notebook](file:/home/lab/NOTEBOOKS/elementary-linear-algebra/notebooks/03_MatrixMultiplication.ipynb)

# 1. The Operations
## 1.1 The Transpose
## 1.2 The Vector Dot Product
## 1.3 The Matrix Product
# 2. Special Cases
## 2.1 Inner and Outer Products
### 2.1.1 Inner Product: Row Vector Times Column Vector
### 2.1.2 Outer Product: Column Vector Times Row Vector
## 2.2 Matrix Times Column Vector, Row Vector Times Matrix
### 2.2.1 Matrix Times Column Vector
### 2.2.2 Row Vector Times Matrix
# 3. Submatrices
## 3.1 We can partition the $A$ and $C$ matrices horizontally
## 3.2 We can partition the $B$ and $C$ matrices vertically
## 3.3 We can partition the $A$ matrix vertically, and the $B$ matrix horizontally
## 3.4 We can partition further, horizontally and vertically...
## 3.5 Test Your Understanding
## 3.6 Notation Convention
# 4. Take Away
