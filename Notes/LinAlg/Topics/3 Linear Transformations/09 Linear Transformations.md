---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[10a Linear Transformations Examples]]
---

09_LinearTransformations.ipynb

# 1. Introduction
## 1.1 Functions Transforming Vectors to Vectors
## 1.2 Geometric Representations
## 1.3 Basic Concepts
## 1.4 Special Case: Linear Transformations
# 2. Useful Theorems
## 2.1 A Linear Transformation Distributes over a Linear Combination
## 2.2 A Linear Transformation from $\mathbb{F}^N \rightarrow \mathbb{F}^M$ Can be Represented by a Matrix
## 2.3 The Composition of Linear Transformations is a Linear Transformation
## 2.4 One-to-one Transformations and Onto Transformations
## 2.5 The Mapping of the $0$ Vector
# 3. Checking Whether a Transformation is Linear
## 3.1 The Test
## 3.2 Examples ( Scalars in $\mathbb{R}$ )
### 3.2.1 A Linear Transformation
### 3.2.2 A Non-linear Transformation
### 3.2.3 The Equation for a Line
# 4. Take Away
