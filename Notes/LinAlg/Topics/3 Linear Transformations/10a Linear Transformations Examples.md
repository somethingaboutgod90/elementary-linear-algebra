---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[10b Linear Tansformation Examples]]
---

10a_LinearTx_Examples.ipynb

# 1. Review: Basic Definitions and Theorems
## 1.1 Definition
## 1.2 One-to-One and ONTO Transformations
## 1.3 Composition of Linear Transformations
# 2. Matrix Representation of a Linear Transformation
## 2.1 Tools: Polar Coordinates and Congruent Triangles
## 2.2 Dilation
## 2.3 A rotation in 2D
## 2.4 Reflection With Respect To a Line
## 2.5 Orthogonal Projection Onto a Line
## 2.6 Combine Linear Transformations
## 2.7 Implementation with Geogebra
# 3. Combining Translations and Linear Transformations
## 3.1 Direct Computation
## 3.2 Homogeneous Coordinates
# 4. Take Away
