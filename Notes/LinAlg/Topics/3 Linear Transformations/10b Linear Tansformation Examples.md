---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[10c Linear Transformations with GeoGebra]]
---

10b_LinearTx_Examples.ipynb

# 1. Application: A Matrix Representation for a Linear Transformation $T: U \rightarrow V$
# 2. Finite Dimensional Vector Spaces Can be Represented by $\mathbb{F}^N$
# 3. A Linear Transformation $T:U\rightarrow V$ Represented by $y = A x$
## 3.1 The Method
## 3.2 Example
### 3.2.1 Verify $T$ is a linear Transformation From a Vector Space to a Vector Space
### 3.2.2 Choose Bases and Coordinate Vector Transforms
### 3.2.3 Obtain the Matrix Representation
### 3.2.4 Using the Matrix for Computations
### 3.2.5 Implementation Using SymPy
# 4. Take Away
