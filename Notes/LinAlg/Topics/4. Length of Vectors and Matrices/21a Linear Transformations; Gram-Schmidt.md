---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[22 QR Decomposition]]
---

21_ProjectionsGramSchmidt.ipynb

# 1. The Normal Equation (Reminder)
# 2. Orthogonal Projection Matrices
## 2.1 Theory
## 2.2 Example: Orthogonal Projection onto a Span of 3 Vectors
## 2.3 Example: Orthogonal Projection onto a Line
## 2.4 Projection onto an Orthogonal Basis
# 3. Orthonormal Bases
## 3.1 Example
## 3.2 Orthogonal Matrices
## 3.3 (Extra Material) A Naive Construction Method for Orthogonal Matrices
### 3.1.3 Two Important Examples
## 3.4 Gramm-Schmidt Orthogonalization
# 4. Take Away
## 4.1 Projection Matrices
## 4.2 The Gram Schmidt Procedure
