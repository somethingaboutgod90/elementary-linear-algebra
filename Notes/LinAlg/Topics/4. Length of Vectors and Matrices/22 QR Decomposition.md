---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'notebook'
tags:   []
up: [[INDEX]]
next: [[23 Metric Spaces]]
---

22_QR_Decomposition.ipynb

# 1. Gram-Schmidt and the QR Decomposition
## 1.1 Reformulate Gram-Schmidt: $\mathbf{A = Q R}$
## 1.2 Computation of $A = Q R$
### 1.2.1 Naive Method
### 1.2.2 Refinement of the Method
### 1.2.3 Alternate Methods
# 2. QR and the Normal Equation
## 2.1 Multiplication by $W^t$ rather than $A^t$
## 2.1 Example
# 3. Take Away
