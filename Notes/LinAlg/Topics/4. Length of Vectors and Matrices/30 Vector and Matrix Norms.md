---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[Rayleigh Quotients]]
---

30_VectorAndMatrixNorms.ipynb

# 1. Introduction
# 2. Vector Norms
# 3. Matrix Norms
## 3.1 Entrywise Vector Norm: $\lVert vec(A) \rVert_{_{_p}}$
## 3.2 Schatten Norm: Vector Norm of the Singular Value $\Sigma$ matrix
## 3.3 Induced Vector Norms
### 3.3.1 Norms induced by $l^{^p}$
## 3.4 Submultiplicative Norms and the Spectral Radius
## 3.4. Example
# 4. Takeaway
