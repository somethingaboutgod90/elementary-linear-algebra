---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'notebook'
tags:   [#fix]
up: [[INDEX]]
next: [[21a Linear Transformations; Gram-Schmidt]]
---

21a_LinearTx_NormalEquations.ipynb

# 1. A Plane through the Origin
## 1.1 Orthogonal Projection Onto a Plane $n \cdot x = 0$ and Onto a Normal to the Plane
## 1.2 The Mirror Image of a Point With Respect to the Plane $n \cdot x = 0$
# 2. A Plane Not Containing the Origin
## 2.1 Orthogonal Projection and Reflection Through a Plane $n \cdot x = b$
# 3.  .FIX.
