---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[14 Determinants]]
---

13_WedgeProduct.ipynb

# 1. The Wedge Product
## 1.1 Changing the Length of a Vector
## 1.2 Anticommutativity
## 1.3 Distributivity over Vector Addition
## 1.4 Generalization to More Vectors
### 1.4.1 Blades
### 1.4.2 Interchanging Vectors, Repeated Vectors
## 1.5 Example Computations
### 1.5.1 Examples with 2 Vectors
### 1.5.2 Example with 3 Vectors
### 1.5.3 General Case with 3 Vectors
## 1.6 Hyper-volumes: the Determinant
# 2. The Determinant Via Laplace Expansion
## 2.1 Using the first Row
## 2.2 Using Different Rows or Columns
## 2.3 Some Easy Determinants
## 2.5 Bad News: Computation is Not Feasible
# 3. Take Away
