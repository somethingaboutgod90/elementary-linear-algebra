---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[17 Eigen Analysis]]
---

14_Determinants.ipynb

# 1. Some Formulae
## 1.1 Laplace Expansion, Leibniz Formula
### 1.1.1 Minors and Cofactors
### 1.1.2 Leibniz Formula
## 1.2 Bilinearity
## 1.3 Scalar and Matrix Products
### 1.3.1 Products of Matrices
### 1.3.2 Theorems and Examples
# 2. The Determinant by Gaussian Elimination
## 2.1 A Practical Algorithm
## 2.2 Existence of the Inverse
# 3. Cramer's Rule, Formula for the Inverse of a Matrix
## 3.1 Solving $\textbf{A x = b}$ with the Wedge Product
### 3.1.1 The Idea
### 3.1.2 A $2 \times 2$ Example
### 3.1.3 General Case
## 3.2 A Formula for the Inverse
# 4. Take Away
