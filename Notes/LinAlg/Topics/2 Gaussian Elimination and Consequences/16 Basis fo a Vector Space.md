---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[09 Linear Transformations]]
---

16_Basis.ipynb

# 1. Basis for a Vector Space
## 1.1 Motivating Example
### 1.1.1 Row Echelon Form of $A$
### 1.1.2 Linearly Independent Columns
#### 1.1.2.1 Removing linearly dependent columns
#### 1.1.2.2 Summary: the reduced system has a unique solution
## 1.2 Basis
### 1.2.1 Definition of a Basis
### 1.2.2 Constructing a Basis from a Given Set of Vectors
## 1.3 Basis and Dimension
### 1.3.1 Definition of the Dimension of a Vector Space
### 1.3.2 The Dimension of $\mathbb{R}^{N}$
### 1.3.3 Example: The Dimension of a Subspace
### 1.3.4 (OMIT) The Dimension of a Subspace of the Vector Space of Functions
### 1.3.5 Some Simple but Useful Theorems to Keep in Mind:
# 2. Fundamental Subspaces Associated with a Matrix
## 2.1 Definitions
## 2.2 A Basis for $\mathscr{C}(A)$
## 2.3 A Basis for $\mathscr{R}(A)$
## 2.4 A Basis for $\mathscr{N}\\ (A)$
## 2.5 A Basis for $\mathscr{N}\\ (A^t)$
# 3. The Fundamental Theorem of Linear Algebra (Part 1)
## 3.1 The Theorem
## 3.2 Examples
### 3.2.1 A matrix of size $1 \times 3$ 
### 3.2.2 A matrix of size $3 \times 3$ 
### 3.2.3 Dimensions of the Fundamental Spaces for a Matrix of Size $4 \times 9$
# 4. Take Away
