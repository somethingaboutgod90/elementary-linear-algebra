---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[16 Basis fo a Vector Space]]
---

15_VectorSpaces.ipynb

# 1. Linear Combinations of Vectors, Vector Spaces
## 1.1 Motivation
## 1.2 Definition
### 1.2.1 Definition of a Vector Space
### 1.2.2 Example of a Computation
## 1.3 Examples and Further Definitions and Theorems
### 1.3.1 Vectors in $\mathbb{R}^N$
#### 1.3.1.1 The Vector Space $V = \\{ 0 \\}$
#### 1.3.1.2 Vectors in a Span of Vectors in $\mathbb{R}^3$ 
### 1.3.2 Closed under Addition, Closed under Scalar Multiplication, Spans
### 1.3.3 Vector Spaces of Matrices
#### 1.3.3.1 The Set of Matrices of the Same Size
#### 1.3.3.2 A Subset of a Set of Matrices of the Same Size 
### 1.3.4 Vector Spaces of Functions
#### 1.3.4.1 The Vector Space of Functions $\mathscr{F}(-\infty,\infty)$
#### 1.3.4.2 The set of Polynomials $\mathscr{P}_2[-1,1]$
### 1.3.5 Set Notation
# 2. Subspaces of a Vector Space
## 2.1. Definition, Subspace Test
## 2.2 Examples
### 2.2.1 Vectors in $\mathbb{R}^N$
#### 2.2.1.1 A Span of Vectors
#### 2.2.1.2 A Set of Vectors that Do Not Contain the Zero Vector 
#### 2.2.1.3 A Set of Vectors that is Not a Span (i.e., Hyperplane)
#### 2.2.1.4 Another Set of Vectors that is Not a Hyperplane
### 2.2.2 Matrices
#### 2.2.2.1 Invertible Matrices
#### 2.2.2.2 Projection Matrices
### 2.2.3 Functions
#### 2.2.3.1 The subspace $\mathscr{C}^2(-1,1)$
#### 2.2.3.2 A Set of Functions that is Not a Subspace
# 3. Take Away
