---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[08 Linear Independence]]
---

07_GE_Systems.ipynb

# 1. Corner Cases for Gaussian Elimination
## 1.1 The Approach to Solving $A x = b$
## 1.2  Missing Pivots
### 1.2.1 Subcase: There is an Equation with Current Leading Variable
### 1.2.2 Subcase: There is No Equation with Current Leading Variable
### 1.2.3 Special Case: Rows of Zeros
# 2. Example Computations
## 2.1 The GE Algorithm
## 2.2 Example: Redundant Equations
## 2.3 Example: Contradiction
## 2.4 Number of Equations, Unknowns and Solutions
# 3. Algorithm Variations
## 3.1 Partial Pivoting, Full Pivoting
## 3.2 Gauss-Jordan Elimination
# 4. Take Away
