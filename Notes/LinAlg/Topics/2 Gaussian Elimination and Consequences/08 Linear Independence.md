---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[11 The Inverse of a Matrix]]
---

08_LinearIndependence.ipynb

# 1. Right Hand Sides $b$
# 2. Solutions of $A x\\ =\\ b$
## 2.1 The Number of Solutions
## 2.2 Particular and Homogeneous Solutions
## 2.3 Non-trivial Homogeneous Solutions
# 3. Linear Independence of Vectors
## 3.1 Definition and Theorem
## 3.2 Examples
### 3.2.1 General Case
### 3.2.2 Special Cases
# 4. Take Away

- - -
**Linear Independence of Functions...**

LinearIndependence.ipynb

# 1. Linear Independence
# 1.1 Model $A x = b$ Problem
# 2. Checking Linear Independence of a set of vectors in $\mathbb{F}^{\small{N}}$
## 2.1 Easy case 1: The set of vectors contains the zero vector
## 2.2 Easy case 2: One of the vectors is recognized as a linear combination of the other vectors.
## 2.3 Easy case 3: More vectors than entries in a vector
# 2.4 Easy case 4: The vectors contain a triangular matrix
# 3: Linear Independence of Functions
## 3.1 Polynomials
