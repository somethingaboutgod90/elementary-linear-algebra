---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[06 Gaussian Elimination - Elementary Operations]]
---

05_RowEchelonForm_Systems.ipynb

# 1. Systems of Linear Equations
## 1.1 Definition
## 1.2 Solutions of a System of Linear Equations
# 2. Solutions of Row Echelon Form Systems
## 2.1 Two Simple Examples
## 2.2 Systems that do NOT have an Equation for Each of the Variables
# 3. The Backsubstitution Algorithm
## 3.1 Definition
## 3.2 The Algorithm
# 4. Take Away
