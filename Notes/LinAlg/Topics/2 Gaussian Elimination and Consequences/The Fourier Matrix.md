---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
---

FourierMatrix.ipynb

# 1. The Discrete Fourier Basis
# 2. Example: Sines Sampled at N=256 Values
## 2.1 Example 1: A Sine Function
## 2.2 Example 2: Sine and a Cosine, Different Frequencies
# 3. A Function of Time
## 3.1 Sample a Function $x(t)$
## 3.2 Removing Some Fourier Coefficients
# 4. Generalization to Higher Dimensional Spaces
# 5. Take Away
