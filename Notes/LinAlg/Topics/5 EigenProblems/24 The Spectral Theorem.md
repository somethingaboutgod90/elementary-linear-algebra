---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[25 Positive Definite Matrices]]
---

24_SpectralTheorem.ipynb

# 1. The Spectral Theorem
## 1.1 Introduction
## 1.2 Normal Matrices
## 1.3 A New Level in the Summary Table
# 2 Example Computations
## 2.1 Example 1
## 2.2 Example 2
# 3. Take Away
