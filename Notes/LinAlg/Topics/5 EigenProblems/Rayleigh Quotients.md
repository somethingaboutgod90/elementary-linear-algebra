---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[The Hamilton  Cayley Theorem]]
---

RayleighQuotients.ipynb

# 1. Computing the Eigenvalue Given an Eigenvector
# 2. Rayleigh Quotient Definition and Theorem
# 3. Numerical Computations
## 3.1 Gradient of the Rayleigh Quotient
## 3.2 Power Method
