---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[19 Diagonalization of Matrices]]
---

18_EigenComputations.ipynb

# 1. Stochastic Matrices
# 2. Null Space Computations
# 3. Matrices of Size $2 \times 2$
# 4. Matrices of Size $3 \times 3$ with a Known Non-zero Eigenvalue
# 5. Determinants that Can be Factored
