---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[18 Eigen Computations]]
---

17_EigenAnalysis.ipynb

# 1.Special Directions for  $\;y\ =\ A\ x$ 
## 1.1 Introduction
### 1.1.1 Examples
## 1.2 The Eigenvector Eigenvalue Problem
### 1.2.1 Definition
### 1.2.2 Checking Potential Eigenvectors
# 2. Solution of $A\ x\ =\ \lambda\ x$
## 2.1 The Characteristic Polynomial
### 2.1.1 Rewrite the Equation into a Familiar Form
### 2.1.2 The Solution of the Eigenproblem
### 2.1.3 Properties of Polynomials
### 2.1.4 Trace and Determinant
## 2.2 Eigenvector/Eigenvalue Computation Examples
### 2.2.1 A Simple $2\times 2$ Example
### 2.2.2 A $3 \times 3$ Example
# 3. Take Away
