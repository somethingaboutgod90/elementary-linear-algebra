---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[27 The Singular Value Decomposition (SVD)]]
---

26_GramMatrix.ipynb

# 1. The Gram Matrix $A^t A$
## 1.1 Rank and the Dimension of the Null Spaces
## 1.2 Dimension of the Eigenspaces for $\lambda = 0$
## 1.3 Relationship of Eigenpairs for $A^t A$ and $A A^t$ for Non-zero Eigenvalues
## 1.4 Orthogonality of Eigenvectors
## 1.5 Eigenvalues Cannot be Negative
## 1.6 Eigenvector Lengths
# 2. Symmetric Eigendecomposition of $A^t A$ and $A A^t$
# 3. Take Away
