---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[29 SVD Applications]]
---

28_PseudoInverse.ipynb

# 1. The Pseudoinverse
## 1.1. A Preimage of $y = A x$
## 1.2 The Pseudoinverse and the Reduced Pseudoinverse
## 1.3 **Projection Matrices**
# 2. Application to the Normal Equation
# 3. Take Away
