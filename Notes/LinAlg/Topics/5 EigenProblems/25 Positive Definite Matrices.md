---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[26 The Gram Matrix]]
---

25_PositiveDefiniteMatrices.ipynb

# 1. Quadratic Forms
## 1.1 Definition
## 1.2 Two Problems: Cross Sections and Extrema
### 1.2.1 **The Extremum Problem**
### 1.2.2 **The Cross Section Problem**
## 1.3. Matrix Formulation
### 1.3.1 Example With 2 Variables
### 1.3.2 General Case
# 2. Orthogonal Decomposition of a Quadratic Form 
## 2.1 Theory
## 2.2 Examples
### 2.2.1. **Example**
### 2.2.2 Example
### 2.2.3 Example
### 2.2.4 Example
## 2.3 Signs of the Eigenvalues are Important
## 2.4 Sylvesters Law of Inertia
# 3. Tests for Positive Definite Matrices
## 3.1 Principal Minors of a Matrix
## 3.2 Positive Definite Matrix Tests
### 3.2.1 Example 1
### 3.2.2 Example 2
### 3.2.3 Example 3
# 4. Take Away
