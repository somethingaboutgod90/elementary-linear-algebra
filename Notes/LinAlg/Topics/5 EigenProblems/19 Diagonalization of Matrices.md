---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[24 The Spectral Theorem]]
---

19_Diagonalization.ipynb

# 1. The Similarity Transform
## 1.1 Change of Basis
## 1.2 Similarity Transform
### 1.2.1 Definition
### 1.2.2 Similar Matrices and Eigenpairs
## 1.3 Special Case: A Basis of Eigenvectors
### 1.3.1 A $2 \times 2$ Example
### 1.3.2 **Should we have predicted this?**
#### 1.3.2.1 **Linearly Independent Basis of Eigenvectors**
#### 1.3.2.2 **The Matrix $A$ Expressed in the Eigenvector Basis is Diagonal**
# 2. Diagonalization
## 2.1 Basic Theory
### 2.1.1 Diagonalizable Matrices
### 2.1.2 Nondiagonalizable Matrices
## 2.2 Special Cases
### 2.2.1 Complex Eigenvalues
### 2.2.2 When is a Matrix Diagonalizable?
#### 2.2.2.1 Existence: No Repeated Eigenvalues
#### 2.2.2.2 Existence: Symmetric Matrices
#### 2.2.2.3 Existence: Normal Matrices
# 3. Applications
## 3.1 Powers of a Diagonalizable Matrix
## 3.2 Functions of a Matrix
# 4. Take Away
