---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[28 The Pseudo Inverse]]
---

27_SVD.ipynb

# 1. Motivation
## 1.1 Generalize the Idea of an Eigendecomposition
## 1.2 Is this Feasible?
## 1.3 The Reduced Singular Value Decomposition
# 2. The Singular Value Decomposition  (SVD)
## 2.1 SVD Existence Theorem
## 2.2 SVD Computation
# 3. Interpretation: the Action of $y = A x$
## 3.1 Change of Coordinate Systems and Scaling
## 3.2 Stretching a Sphere
# 4. Take Away
