---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[Iterative Methods]]
---

HamiltonCayley.ipynb

# 1. The Hamilton Cayley Theorm and the Inverse of a Matrix
## 1.1 Preliminary
## 1.2 Example
## 1.3 The Cayley Hamilton Theorem
## 1.4 Application: a Formula for the Inverse
### 1.4.1 General $2 \times 2$ matrix
### 1.4.2 Continuation of the $3 \times 3$ Example
