---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: ''
status: 'SoSo'
tags:   []
up: [[INDEX]]
next: [[30 Vector and Matrix Norms]]
---

29_SVDapplications.ipynb

# 1. The Eckart-Young Theorem
# 2. Low Rank Approximation of an Image
# 3. Principal Component Analysis
# 4. Some Web Resources
