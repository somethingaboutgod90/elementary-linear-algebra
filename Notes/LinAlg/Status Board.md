---

kanban-plugin: basic

---

## create notebook

- [ ] 01 Scalars, Vectors, Matrices
- [ ] 02 Addition, Scalar Multiplication, Dot Product
- [ ] 03 Matrix Multiplication
- [ ] 04 Vector and Matrix Algebra
- [ ] Matrix Multiplication Summary
- [ ] 05 Rowechelon Form Systems
- [ ] 06 GE - Elementary Operations
- [ ] 07 GE Algorithm
- [ ] 08 Linear Independence
- [ ] 11 Inverse of a Matrix
- [ ] 12 LU Decomposition
- [ ] 15 Vector Spaces
- [ ] 16 Basis of a Vector Space
- [ ] other Three Bases Example
- [ ] The Fourier Basis
- [ ] 09 Linear Transformation
- [ ] 10a Linear Transformation Examples
- [ ] 10b Linear Transformation Examples
- [ ] 10c Linear Transformations with Geogebra
- [ ] other GeoGebra
- [ ] other Projections
- [ ] 13 The Wedge Product
- [ ] 14 Determinants
- [ ] 20 Length and Orthogonality
- [ ] 21 Linear Transformations, Normal Equations
- [ ] 21a Linear Transformations, Gram Schmidt
- [ ] 22 QR Decomposition
- [ ] 23 Metric Spaces
- [ ] 30 Vector and Matrix Norms
- [ ] other Least Mean Squares
- [ ] other Least Squares Fitting
- [ ] Regression
- [ ] 17 Eigenanalysis
- [ ] 18 Eigencomputations
- [ ] 19 Diagonalization of Matrices
- [ ] 24 The Spectral Theorem
- [ ] 25 Positive Definite Matrices
- [ ] 26 The Gram Matrix
- [ ] 27 The SVD
- [ ] 28 The Pseudoinverse
- [ ] 29 SVD Applications
- [ ] Rayleigh Quotient
- [ ] The Hamilton CayleyTheorem
- [ ] Iterative Methods
- [ ] Iterative Methods with Julia
- [ ] Iterative Methods with Python
- [ ] other Iteration Examples
- [ ] Randomized SVD
- [ ] A1 Analytic Geometry
- [ ] A2 Matrix Layout Displays
- [ ] GE Layout Display
- [ ] Generate Problems


## create video



## youtubepublished



## 





%% kanban:settings
```
{"kanban-plugin":"basic"}
```
%%