---
title: HowTo
tags:  #HowTo
topic: instructions
up: [[INDEX]]
---

# HowTo
## adding Notes, inserting links
- Add a new note:  `<Alt n>/ff Add Note`
- Add a webpage link to a note: `<Alt e>/ff Add from Clipboard`

## open a notebook from obsidian
- Open a notebook:
    - execute 
    ```bash
      jupyter lab --no-browser &
    ```
    - then click on links of the form
    ```text
        [lecture 1]
        (http://localhost:8888/lab/tree/NOTEBOOKS/.../01_ScalarsVectorsMatrices.ipynb)
    ```

[lecture 1](http://localhost:8888/lab/tree/NOTEBOOKS/elementary-linear-algebra/notebooks/01_ScalarsVectorsMatrices.ipynb)


## Obsidian Media Extended: The Best Way To Take Notes On Videos 🎥️ - YouTube
▬▬▬▬▬▬▬▬▬▬ ► CHECK THESE OUT ◀︎▬▬▬▬▬▬▬▬▬▬📧️ NEWSLETTER: https://bryanjenkstech.ck.page/d4ec0713d5💬 DISCORD:  https://discord.gg/MxCVshN🗣️ SOCIALS:  https:...
[Obsidian Media Extended: The Best Way To Take Notes On Videos 🎥️ - YouTube](https://www.youtube.com/watch?v=GQXVWtNkeZw)
