{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "%load_ext itikz\n",
    "import itikz\n",
    "from itikz import nicematrix as nM\n",
    "import jinja2\n",
    "\n",
    "import numpy as np\n",
    "import sympy as sp\n",
    "import clifford as cf\n",
    "from clifford.g4 import *\n",
    "\n",
    "import sympy as sym"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align:center;\"><strong style=\"height:100px;color:darkred;font-size:40px;\">The Determinant (Part b)</strong><br>\n",
    "    <strong style=\"height:100px;color:darkred;font-size:30px;\">Formulae and Computation</strong>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import YouTubeVideo\n",
    "YouTubeVideo(\"mr4UgDPLO0I\", 400, 200, frameborder=\"0\",\n",
    "      allow=\"accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture\",\n",
    "      allowfullscreen=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Some Formulae"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1 Laplace Expansion, Leibniz Formula"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1.1 Minors and Cofactors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;background-color:#F2F5A9;color:black;padding-right:0.3cm;\">\n",
    "\n",
    "**Definition:** The **minor** $A_{i j}$ of a matrix $A$ is the determinant\n",
    "of the matrix that results<br>$\\quad$ from deleting the $i^{th}$ row and the $j^{th}$ column of $A$<br><br>\n",
    "$\\quad$ The cofactor $C_{i j}$ of a matrix $A$ is\n",
    "$$ C_{i j} =(-1)^{i+j} A_{i j}$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Laplace expansion of the determinant of a matrix $A$ of size $N \\times N$ is given by<br><br>\n",
    "$$\\begin{align}\n",
    "\\left| A \\right|\n",
    "&= \\sum_{j=1}^N { (-1)^{i+j} a_{i j} A_{i j}} &=& \\sum_{j=1}^N { a_{i j} C_{i j} },\n",
    "         \\qquad \\text{ for any given row    } i \\;\\; \\text{(row expansion)} \\\\\n",
    "&= \\sum_{i=1}^N { (-1)^{i+j} a_{i j} A_{i j}} &=& \\sum_{i=1}^N { a_{i j} C_{i j}},\n",
    "         \\qquad \\text{ for any given column } j \\;\\; \\text{(column expansion)}\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Example:**\n",
    ">\n",
    "> $$\\begin{align}\n",
    "\\left| A \\right| =  \\left| \\begin{matrix}\n",
    "              1 &            2 &            4 &            3 \\\\\n",
    "   \\color{red}0 & \\color{red}1 & \\color{red}3 & \\color{red}0 \\\\\n",
    "              2 &            0 &           -1 &            0 \\\\\n",
    "              1 &            1 &            0 &            0  \\end{matrix} \\right|\n",
    "&= -\\color{red}0^ \\ \\left| \\begin{matrix}\n",
    "              2 &            4 &            3 \\\\\n",
    "              0 &           -1 &            0 \\\\\n",
    "              1 &            0 &            0    \\end{matrix} \\right|\n",
    "   +\\color{red}1 \\ \\left| \\begin{matrix} 1 &  4 & 3 \\\\\n",
    "                                         2 & -1 & 0 \\\\ 1 & 0 &  0 \\end{matrix} \\right|\n",
    "   -\\color{red}3 \\ \\left| \\begin{matrix} 1 &  2 & 3 \\\\ 2 & 0 &  0 \\\\ 1 & 1 & 0 \\end{matrix} \\right|\n",
    "   +\\color{red}0 \\ \\left| \\begin{matrix} 1 &  2 & 4 \\\\ 2 & 0 & -1 \\\\ 1 & 1 & 0 \\end{matrix} \\right|\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:55%;padding-right:1cm;\">\n",
    "\n",
    "> The expansion uses the $2^{nd}$ row:\n",
    ">\n",
    "> <div style=\"border-right:2px solid black;padding-right:1cm;height:1.7cm;\">\n",
    "$\\quad \\begin{align} \\left| A \\right| &=\\; - \\color{red}0\\ A_{2 1} + \\color{red}1\\ A_{2 2} - \\color{red}3\\ A_{2 3} + \\color{red}0 A_{2 4} \\\\\n",
    "&=\\; \\;\\; \\color{red}0\\ C_{2 1} + \\color{red}1\\ C_{2 2} + \\color{red}3\\ C_{2 3} + \\color{red}0\\ C_{2 4}\n",
    "\\end{align}$\n",
    "</div>\n",
    "</div>\n",
    "<div style=\"float:left;padding-left=2cm;width:30%;\">\n",
    "<br><br>\n",
    "$\\begin{align} A_{2 1} &= 1 \\left| \\begin{matrix} 4 & 3 \\\\ -1 & 0 \\end{matrix} \\right| &=& 3 \\\\\n",
    "                      C_{2 1} &= - \\left|A_{2 1} \\right| &=& -3 \\end{align}$\n",
    "</div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1.2 Leibniz Formula"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:46%;padding-right:1cm;\">\n",
    "\n",
    "Let $a_i = \\alpha_{i 1} e_1 + \\alpha_{i 2} e_2 + \\alpha_{i n} e_n$ for $i$ in $1,2, n$.\n",
    "<br><br>\n",
    "The wedge product $a_1 \\wedge a_2 \\dots \\wedge a_n$ consists of <strong>all possible terms</strong><br>\n",
    "* consisting of **one entry from each** of the $\\mathbf{a_i}$ vectors<br>\n",
    "* each with **distinct entries** $\\mathbf{e_j}$\n",
    "\n",
    "i.e., <strong>one entry from every row and every column<br>\n",
    "    $\\quad$ of the coefficient matrix</strong>\n",
    "\n",
    "Each term has a **plus or minus sign** due to the reordering<br>\n",
    "$\\quad$ of the $e_i$ in numerical order.\n",
    "</div>\n",
    "<div style=\"float:left;width:40%;border-left:2px solid black;padding-left:1cm;\">\n",
    "\n",
    "For example,\n",
    "$$\\left| \\begin{matrix}\n",
    "          \\alpha_{1 1}  & \\boxed{\\alpha_{1 2}} & \\alpha_{1 3}         &         \\alpha_{1 4}  \\\\\n",
    "          \\alpha_{2 1}  &        \\alpha_{2 2}  & \\alpha_{2 3}         & \\boxed{ \\alpha_{2 4}} \\\\\n",
    "   \\boxed{\\alpha_{3 1}} &        \\alpha_{3 2}  & {\\alpha_{3 3}}       &         \\alpha_{3 4}  \\\\\n",
    "          \\alpha_{4 1}  &        \\alpha_{4 2}  & \\boxed{\\alpha_{4 3}} &         \\alpha_{4 4}\n",
    "\\end{matrix} \\right|$$\n",
    "<br><br>\n",
    "corresponds to the term\n",
    "$$\\begin{align}\\zeta =&\\;\\;\\;\\;\\ \\alpha_{1 2}\\ \\alpha_{2 4}\\ \\alpha_{3 1}\\ \\alpha_{4 3}\\; \\color{red}{e_{2 4 1 3}} \\\\\n",
    " =& -                  \\; \\alpha_{1 2}\\ \\alpha_{2 4}\\ \\alpha_{3 1}\\ \\alpha_{4 3}\\; \\color{red}{e_{1 2 3 4}}\n",
    "\\end{align}$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:46%;padding-right:1cm;\">\n",
    "\n",
    "**Leibniz Formula for the Determinant**\n",
    "\n",
    "$$\\quad { \\left| A \\right| = \\sum_{j_1,j_2,\\dots j_n}{ \\epsilon_{j_1 j_2\\dots j_n}\\; \\alpha_{1 j_1} \\alpha_{2 j_2} \\dots  \\alpha_{n j_n}  }}$$\n",
    "\n",
    "the [**Levi-Civita Density**](https://wikimili.com/en/Levi-Civita_symbol) $\\;\\epsilon_{j_1 j_2\\dots j_n}\\;$\n",
    "is defined by<br>\n",
    "$\\quad$ $e_{j_1 j_2 \\dots j_n} = \\epsilon_{j_1 j_2\\dots j_n}\\; e_{1 2 \\dots n}$ for distinct values of the indices,<br>\n",
    "$\\quad$ it is zero otherwise.\n",
    "</div>\n",
    "<div style=\"float:left;width:40%;height:4.3cm;border-left:2px solid black;padding-left:1cm;\">\n",
    "  <br>\n",
    "    In the example above, $\\; \\epsilon_{2 4 1 3} = -1$<br><br>\n",
    "    $\\qquad e_{2 4 \\color{red}1 3} = e_{1 2 4 \\color{red}3} = - e_{1 2 3 4}$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2 Bilinearity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we express the determinant of a matrix $A$ as a function of the rows/cols,<br>\n",
    "$\\quad \\Delta( r_1, r_2, \\dots r_N )$ the function is **bilinear**,<br>\n",
    "$\\quad$ i.e., **linear** for each of the arguments:\n",
    "\n",
    "$\\qquad   \\Delta( r_1,\\dots,\\color{red}{\\alpha r_i+\\beta \\tilde{r}_i}, \\dots r_N )\n",
    "\\;=\n",
    "\\;\\color{red}{\\alpha}\\ \\Delta( r_1,\\dots,\\color{red}{r_i}, \\dots r_N )\n",
    "+ \\color{red}{\\beta}\\  \\Delta( r_1,\\dots,\\color{red}{\\tilde{r}_i}, \\dots r_N )$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Examples**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;padding-right:1cm;\">\n",
    "<strong>Factoring out a Constant from a Row or Column</strong><br><br>\n",
    "$$\\begin{align}\n",
    "\\left| \\begin{array}{rrr} 2 & 3 & 4 \\color{red}x \\\\ 3 & 1 & -\\color{red}x \\\\ 0 & 2 & 8\\color{red}x \\end{array} \\right|\n",
    "=&\\;\\;\\ \\color{red}x \\left| \\begin{array}{rrr} 2 & 3 & 4 \\\\ 3 & 1 & -1 \\\\ \\color{red}0 & \\color{red}2 & \\color{red}8 \\end{array}\\right| \\\\\n",
    "=&\\ \\color{red}2 x \\left| \\begin{array}{rrr} 2 & 3 & 4 \\\\ 3 & 1 & -1 \\\\ 0 & 1 & 4 \\end{array}\\right| \\\\\n",
    "\\end{align}$$\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:1cm;border-left:2px solid black;\">\n",
    "<strong>Splitting a Row or Column</strong><br><br>\n",
    "$$\\begin{align}\n",
    "\\left| \\begin{array}{rrr} 2 & 3 & \\color{red}{x+2y} \\\\ 3 & 1 & \\color{red}{x - y} \\\\ 0 & 2 & \\color{red}{8x} \\end{array} \\right|\n",
    "=&\\;  \\left| \\begin{array}{rrr} 2 & 3 & x \\\\ 3 & 1 & x \\\\ 0 & 2 & 8x \\end{array}\\right|\n",
    "\\;+\\;  \\left| \\begin{array}{rrr} 2 & 3 & 2y \\\\ 3 & 1 & -y \\\\ \\color{red}0 & \\color{red}2 & \\color{red}0 \\end{array}\\right|\n",
    "\\\\\n",
    "=&\\;  \\left| \\begin{array}{rrr} 2 & 3 & x \\\\ 3 & 1 & x \\\\ 0 & 2 & 8x \\end{array}\\right|\n",
    "\\;+\\;  \\left| \\begin{array}{rrr} 2 & 3 & 2y \\\\ 3 & 1 & -y \\\\ 1 & 1 & 0 \\end{array}\\right|\n",
    "\\;+\\;  \\left| \\begin{array}{rrr} 2 & 3 & 2y \\\\ 3 & 1 & -y \\\\ -1 & 1 & 0 \\end{array}\\right|\n",
    "\\end{align}$$</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.3 Scalar and Matrix Products"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3.1 Products of Matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Algebraic Derivation**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:46%;padding-right:1cm;\">\n",
    "Consider the sets of $i=1,2, \\dots n$ vectors\n",
    "$$\\begin{align} a_i &= \\sum_{j=1}^n \\alpha_{i j} {e_j} \\\\\n",
    "               e_j &= \\sum_{k=1}^n \\beta_{j k} { \\tilde{e}_k} \\end{align}$$\n",
    "</div>\n",
    "<div style=\"float:left;width:45%;height:3.4cm;padding-left:1cm;border-left:2px solid black;\">\n",
    "\n",
    "i.e., a set of vectors $\\mathbf{a_i}$ expressed<br>$\\quad$ as linear combinations of vectors $\\mathbf{e_i}$,<br><br>\n",
    "$\\quad$ that in turn are expressed<br>$\\quad$ as linear combinations of vectors $\\mathbf{\\tilde{e}_i}$.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:46%;padding-right:1cm;\">\n",
    "Substituting, we obtain $$ a_i = \\sum_{j=1}^n \\alpha_{i j} \\sum_{k=1}^n \\beta_{j k} \\tilde{e}_k\n",
    "= \\sum_{k=1}^n { \\left( \\sum_{j=1}^n {\\alpha_{i j} \\beta_{j k}} \\right) \\tilde{e}_k }$$\n",
    "</div>\n",
    "<div style=\"float:left;width:45%;height:2.2cm;padding-left:1cm;border-left:2px solid black;\">\n",
    "Substitution shows the coefficients of the $\\mathbf{a_i}$<br>$\n",
    "\\quad$ with respect to the vectors $\\mathbf{ \\tilde{e}_j }$ <br>\n",
    "$\\quad$ are the matrix product $\\left( \\alpha_{i j} \\right)\\left( \\beta_{i j} \\right)$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Rewrite using Matrix Notation**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:50%;\">\n",
    "Using matrix notation, set\n",
    "$$a = \\left( a_i \\right), \\; e = \\left( e_i \\right),\\; \\tilde{e} = \\left( \\tilde{e}_i \\right)$$\n",
    "$$A = \\left( \\alpha_{i j} \\right), \\; B = \\left( \\beta_{i j} \\right)$$\n",
    "</div>\n",
    "<div style=\"float:left;width:40%;height:2.2cm;padding-left:1cm;border-left:2px solid black;\">\n",
    "Then\n",
    "$$\\left. \\begin{align}\n",
    "a &= A\\ e \\\\\n",
    "e &= B\\ \\tilde{e}\n",
    "\\end{align} \\right\\} \\; \\Rightarrow a = (A B)\\ \\tilde{e}\n",
    "$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Example**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"padding-left:1cm;\">\n",
    "$\\left\\{\\begin{align} a_1 =&\\         2 e_1 + 3         e_2 \\\\\n",
    "                        a_2 =&\\         4 e_1 + 2         e_2 \\end{align}\\right.,$ $\\quad$\n",
    "  $\\left\\{\\begin{align}e_1 =&\\;\\;\\;\\;\\tilde{e}_1 + 2 \\tilde{e}_2 \\\\\n",
    "                       e_2 =&\\ - \\tilde{e}_1 - 3 \\tilde{e}_2 \\end{align}\\right.$ $\\quad \\Rightarrow\\quad$\n",
    "  $\\left\\{\\begin{align}\n",
    "  a_1 =&\\ - \\tilde{e}_1 - 5 \\tilde{e}_2\\\\\n",
    "  a_2 =&\\;\\ \\; 2 \\tilde{e}_1 + 2 \\tilde{e}_2\n",
    "  \\end{align}\\right.$ $\\quad$\n",
    "\n",
    "where<br><br>\n",
    "  $\\begin{pmatrix} 2 & 3\\\\ 4 & 2 \\end{pmatrix}\n",
    "   \\begin{pmatrix} 1 & 2\\\\ -1 & -3 \\end{pmatrix} \\ = \\\n",
    "   \\begin{pmatrix} -1 & -5\\\\ 2 & 2 \\end{pmatrix}\n",
    "  $\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Main Formula**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<!--%%itikz --temp-dir --keep det_AB --file-prefix det_  --template article --tex-packages=cascade,amsmath --crop\n",
    "%--use-dvi --use-xetex\n",
    "\n",
    "\\Cascade%\n",
    "{%\n",
    "   \\ShortCascade{$a_1  \\dots \\wedge a_n = \\left| A \\right| \\ e_{1 2 \\dots n}\\;$}\n",
    "                {$e_1  \\dots \\wedge e_n = \\left| B \\right| \\ \\tilde{e}_{1 2 \\dots n}\\;$}\n",
    "}\n",
    "{$ \\Rightarrow  a_1  \\dots \\wedge a_n = \\left| A \\right| \\left| B \\right|  \\ \\tilde{e}_{1 2 \\dots n}$}\n",
    "{$a_1 \\wedge a_2 \\dots \\wedge a_n = \\left| A B \\right|  \\ \\tilde{e}_{1 2 \\dots n}$}\n",
    "{}\n",
    "{$\\Rightarrow \\boxed{ \\left| A B \\right| = \\left| A \\right| \\left| B \\right|}$ }\n",
    "-->\n",
    "<div style=\"padding-left:1cm;\"><img src=\"Figs/det_AB.svg\" width=600></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3.2 Theorems and Examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Special Cases of the Product Formula**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;padding-right:2cm;width:40%;\">\n",
    "    \n",
    "$\\quad \\begin{align} \\left| \\alpha A \\right| =&\\ \\left|\\ \\alpha I_N\\ A \\right| \\qquad \\text{where } A \\text{ has size } N \\times N\\\\\n",
    "                                       =&\\ \\left|\\ \\alpha I_N \\right|\\ \\left| A \\right| \\\\\n",
    "                                       =&\\ \\alpha^N \\left| A \\right| \\end{align}$\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:2cm;border-left:2px solid black;height:2.1cm;\">\n",
    "\n",
    "$\\begin{align} \n",
    "\\left| A^k \\right| =& \\left| A\\ A \\dots A \\right| \\\\\n",
    "                   =& \\left| A \\right| \\left| A \\dots A \\right|\\\\\n",
    "                   =& \\left| A \\right|^{k}\n",
    "\\end{align}$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Summarize the Theorems**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;background-color:#F2F5A9;color:black;padding-right:0.3cm;\">\n",
    "\n",
    "**Theorem** Let $A$ and $B$ be square matrices of size $N \\times N$\n",
    "* $\\left| A B \\right| = \\left| A \\right| \\left| B \\right| \\qquad\\qquad\\qquad\\qquad\\qquad\\qquad(1)$\n",
    "* $\\left| A ^k\\right| = \\left| A \\right|^k\\;$ for any positive integer $k \\qquad\\qquad\\quad(2)$<br>\n",
    "  $\\qquad\\qquad\\;\\;$ **Remark:** If $A^{-1}$ exists, this generalizes to any integer $k$, positive or negative\n",
    "* $\\left| \\alpha A \\right| = \\alpha^N \\left| A \\right|  \\;\\qquad\\qquad\\qquad\\qquad\\qquad\\qquad(3)$\n",
    "* $\\left| A^t \\right| = \\left| A \\right| \\qquad\\qquad\\qquad\\qquad\\qquad\\qquad\\qquad(4)$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **The Determinant of the Inverse Matrix**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;padding-right:1cm;\">\n",
    "\n",
    "$\\quad$\n",
    "$\\begin{align}A^{-1} A = I \\Rightarrow &\\; \\left| A^{-1} A \\right| = 1 \\\\\n",
    "\\Leftrightarrow &\\; \\left| A^{-1} \\right| \\left| A \\right| = 1 \\\\\n",
    "\\Leftrightarrow &\\; \\left| A^{-1} \\right| = \\frac{1}{\\left| A \\right| } = \\left| A \\right|^{-1}\n",
    "\\end{align}$\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:1cm;border-left:2px solid black;height:3.3cm;\">\n",
    "\n",
    "**Remark:** For the division to hold, we need $\\left| A \\right| \\ne 0$<br>\n",
    "    $\\quad$ we will show this below.\n",
    "\n",
    "**Example:**\n",
    "    Given $\\left| A \\right| = 10$ we conclude<br>\n",
    "    $\\quad \\begin{align} \\left| A^{-1} \\right| = \\frac{1}{10}\\end{align}$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Typical Problems**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Example 1: Simplify an Expression**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;padding-right:1cm;\">\n",
    "\n",
    "Given a matrix $A$ of size $5 \\times 5$ with determinant $\\left| A \\right| = 3$<br><br>\n",
    "$\\quad$ Find the determinant $\\;\\;\\left|\\ 2\\ A^{1021} A^t A^{-1} \\right|$\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:1cm;border-left:2px solid black;height:4.3cm;\">\n",
    "\n",
    "$\\quad$\n",
    "$\\begin{align}\n",
    "\\Delta \\;=& \\;\\left|\\ 2\\ A^{1021} A^t A^{-1} \\right| \\\\\n",
    "         =& \\;2^5\\ \\left| A^{1021} A^t A^{-1} \\right| \\;\\;\\quad\\qquad \\text{ using Eq(3)}\\\\\n",
    "         =& \\;2^5\\ \\left| A^{1021} \\right|\\ \\left| A^t \\right|\\ \\left|  A^{-1} \\right| \\qquad \\text{using Eq(1)}\\\\\n",
    "         =& \\;2^5\\ \\left| A \\right|^{1021} \\ \\left| A \\right| \\ \\left|  A \\right|^{-1}  \\qquad \\text{using Eq(2) and Eq(4)}\\\\\n",
    "         =& \\;2^5\\ \\left| A \\right|^{1021} \\qquad\\qquad\\qquad \\text{canceling reciprocal scalars}\\\\\n",
    "         =& \\;2^5\\ 3^{1021}\n",
    "\\end{align}$ \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Example 2: The Determinant of a Projection Matrix**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Projection matrices $P$ satisfy $P^2 = P$<br>\n",
    "$\\quad$ Let $\\;\\Delta = \\left| P \\right|\\;$ and take the determinant of both sides, we have<br><br>\n",
    "$\\quad \\begin{align}\n",
    "\\left| P^2 \\right| = \\left| P\\right| \\Leftrightarrow &\\; \\left| P \\right|^2 = \\left| P \\right| \\\\ \n",
    "                                     \\Leftrightarrow &\\; \\Delta^2 - \\Delta = 0 \\\\\n",
    "                                     \\Leftrightarrow &\\; \\Delta ( \\Delta - 1 ) = 0\n",
    "\\end{align}$\n",
    "\n",
    "$\\therefore$ The determinant of a projection matrix is either $1$ or $0$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. The Determinant by Gaussian Elimination"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.1 A Practical Algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**The Product Formula Lets us Compute a Determinant By Gaussian Elimination!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<!--\n",
    "Ab = sym.Matrix([[1,2,1],[2,4,4],[0,1,1]]); matrices = [[None, Ab]]; pivots = []; txt=[]\n",
    "E1 = sym.eye(3);E1[1:,0]=[-2,0]; A1=E1*Ab;                                matrices.append([E1,A1]); pivots.append((1,1));txt.append('A')\n",
    "E2 = sym.eye(3);E2=E2.elementary_row_op('n<->m',row1=1,row2=2); A2=E2*A1; matrices.append([E2,A2]); pivots.append(None); txt.append('$A_1 = E_1 A$')\n",
    "E3 = sym.eye(3);E3[2,2]=1/2; A3=E3*A2;                                    matrices.append([E3,A3]); pivots.append((2,2)); txt.append('$A_2 = E_2 E_1 A$')\n",
    "pivots.append((3,3)); txt.append('$A_3 = E_3 E_2 E_1 A$')\n",
    "\n",
    "mat_rep, submatrix_locs, pivot_locs, txt_with_locs,mat_format = nM.ge_layout( matrices, Nrhs=0, pivots=pivots, txt=txt, formater=lambda x: sym.latex(x))\n",
    "\n",
    "itikz.fetch_or_compile_svg( jinja2.Template( nM.GE_TEMPLATE ).render( preamble=nM.preamble, extension=nM.extension,\n",
    "                                                                      mat_rep=mat_rep, mat_format=mat_format, submatrix_locs=submatrix_locs, pivot_locs=pivot_locs, txt_with_locs=txt_with_locs),\n",
    "                            prefix='det_ge_', working_dir='/tmp/itikz', debug=False,\n",
    "                            **itikz.build_commands_dict(use_xetex=True,use_dvi=False,crop=True), nexec=4, keep_file='det_using_GE' )\n",
    "-->\n",
    "<div style=\"float:left;padding-right:1cm;width:30%;padding-left:1cm;\">\n",
    "\n",
    "<img src=\"Figs/det_using_ge.svg\">\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:1cm;border-left:2px solid black;height:6.3cm;width:55%;\">\n",
    "\n",
    "We can **use the product formula** at each level of this computation\n",
    "\n",
    "* $\\left| E_1 A \\right| = \\left| A_1 \\right|\n",
    "   \\qquad\\quad \\Rightarrow \\left| A \\right| \\;=\\; \\frac{ \\left| A_1 \\right| }{ \\left| E_1 \\right| } \\;=\\;  \\frac{-2}{1} = -2\n",
    "$\n",
    "\n",
    "* $\\left| E_2 E_1 A \\right| = \\left| A_2 \\right|\n",
    "   \\quad\\quad \\Rightarrow \\left| A \\right| \\;=\\; \\frac{ \\left| A_2 \\right| }{ \\left| E_1 \\right|  \\left| E_2 \\right| } \\;=\\; \\frac{2}{1 \\times (-1)} = -2\n",
    "$\n",
    "\n",
    "* $\\left| E_3 E_2 E_1 A \\right| = \\left| A_3 \\right|\n",
    "   \\quad \\Rightarrow \\left| A \\right| \\;=\\; \\frac{ \\left| A_3 \\right| }{ \\left| E_1 \\right|  \\left| E_2 \\right|   \\left| E_3 \\right| } \\;=\\; \\frac{1}{1 \\times (-1) \\times \\frac{1}{2}} = -2\n",
    "$\n",
    "\n",
    "**Remark:** Once $A_k$ is in row echelon form,<br>\n",
    "    $\\quad$ $\\left| A_k \\right|\\;$\n",
    "    **is the product of the diagonal entries.**\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Remarks**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Suppose we do **not use scaling** and there is a **pivot in every column**<br>\n",
    "    $\\quad$ then $\\left| A \\right| = (-1)^q \\prod_{k=1}^N { p_k}$<br>\n",
    "    $\\quad$ where $q$ is the number of row exchange matrices used<br>\n",
    "    $\\quad$ and the $p_k$ are the pivots in the row echelon form<br><br>\n",
    "    i.e. **the determinant is the product of the pivots** up to a plus or minus sign"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* If we do **use scaling**, the scale factors must be accounted for:<br>\n",
    "$\\quad$ this is accomplished when we divide out the determinant of the scaling matrix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Missing Pivot?\n",
    "    * If there is a **missing pivot,** the row echelon form matrix will have a zero on the diagonal.<br>\n",
    "    $\\quad$ if there is a **missing pivot**, then $\\; \\mathbf{\\left| A \\right| = 0}$\n",
    "    * if **every column has a pivot,** then $\\;\\; \\mathbf{\\left| A \\right| \\ne 0}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2 Existence of the Inverse"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;background-color:#F2F5A9;color:black;padding-right:0.3cm;\">\n",
    "\n",
    "**Theorem:** a square matrix $A$ is invertible iff $\\left| A \\right| \\ne 0$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Cramer's Rule, Formula for the Inverse of a Matrix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.1 Solving $\\textbf{A x = b}$ with the Wedge Product"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1.1 The Idea"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the wedge product to solve $A x = b$\n",
    "\n",
    "$\\quad$ We will **limit the discussion to square invertible matrices** $\\mathbf{A}:$<br>\n",
    "$\\quad$ Let $a_i$ be the columns of $A$. In **column view**<br><br>\n",
    "$\\qquad\\quad A x = b \\Leftrightarrow x_1 a_1 + x_2 a_2 + \\dots x_n a_n = b$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1.2 A $2 \\times 2$ Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let\n",
    "$\n",
    "\\;x_1 a_1 + x_2 a_2 = b\n",
    "$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;padding-right:1cm;width:60%;\">\n",
    "<hr>\n",
    "If we <strong>take the wedge product</strong> of this equation with $a_2$,<br>\n",
    "$\\qquad$ the $x_2$ term drops out:<br><br>\n",
    "$\n",
    "\\qquad x_1\\ a_1 \\wedge a_2 + x_2\\ \\color{red}{ a_2 \\wedge a_2} = b \\wedge a_2\n",
    "\\;\\; \\Leftrightarrow \\;\\; x_1 \\ \\left| a_1 \\; a_2 \\right|\\; e_{1 2} = \\left| b\\;\\; \\; a_2 \\right|\\; e_{1 2}\n",
    "$\n",
    "<br>\n",
    "<hr>\n",
    "Similarly, if we <strong>take the wedge product</strong> of this equation with $a_1$,<br>\n",
    "$\\qquad$ the $x_1$ term drops out:<br><br>\n",
    "$\n",
    "\\qquad x_1\\ \\color{red}{a_1 \\wedge a_1} + x_2\\ { a_2 \\wedge a_1} = b \\wedge a_1\n",
    "\\;\\; \\Leftrightarrow \\;\\; x_2 \\ \\left| a_1 \\; a_2 \\right| e_{1 2} = \\left| a_1 \\;\\ b \\right|\\; e_{1 2}\n",
    "$\n",
    "\n",
    "\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:1cm;border-left:2px solid black;width:30%;height:6.5cm;\">\n",
    "\n",
    "<hr>\n",
    "We have formulae for $x_1$ and $x_2$:<br><br>\n",
    "\n",
    "$$\n",
    "x_1\\ = \\ \\frac{  \\left| \\color{red}b \\; a_2  \\right| }{ \\left| \\color{red}{a_1} \\; a_2 \\right|}\n",
    "$$\n",
    "\n",
    "<hr><br><br>\n",
    "$$\n",
    "x_2\\ = \\ \\frac{  \\left| a_1 \\; \\color{red}b  \\right| }{ \\left| a_1 \\; \\color{red}{a_2} \\right|}\n",
    "$$\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Numerical Example and Geometric Interpretation**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"height:5cm;float:left;\">\n",
    "$\\quad \\begin{pmatrix} 2 & 2 \\\\ 6 & -3 \\end{pmatrix} \\begin{pmatrix} x_1 \\\\ x_2 \\end{pmatrix} = \n",
    "\\begin{pmatrix} \\color{red}6 \\\\ \\color{red}9 \\end{pmatrix}\n",
    "\\quad \\Rightarrow \\quad\n",
    "\\left\\{ \\begin{align}\n",
    "x_1 =& \\frac{\\left| \\begin{array}{rr} \\color{red}6 & 2 \\\\ \\color{red}9 & -3 \\end{array}\\right| }{\\left| \\begin{array}{rr} 2 & 2 \\\\ 6 & -3 \\end{array}\\right| } \\;=\\; \\frac{-36}{-18} \\; =\\;  2 \\\\\n",
    "x_2 =& \\frac{\\left| \\begin{array}{rr} 2 &  \\;\\;\\color{red}6 \\\\ 6 & \\color{red}{9} \\end{array}\\right| }{\\left| \\begin{array}{rr} 2 & 2 \\\\ 6 & -3 \\end{array}\\right| } \\;=\\; \\frac{-18}{-18} \\;=\\; 1\n",
    "\\end{align}\\right.\n",
    "$\n",
    "</div>\n",
    "<div style=\"height:5.5cm;float:left;padding-left:2.5cm;\">\n",
    "<img src=\"Figs/det_cramer_slide.svg\" width=300>\n",
    "</div>\n",
    "<!--\n",
    "%%itikz --keep det_cramer_slide --temp-dir --file-prefix cramer- --template pic --use-xetex --scale 1 --tex-packages=amsmath --tikz-libraries=calc\n",
    "\\definecolor{la_white}{RGB}{233,235,223} %#E9EBDF\n",
    "\\definecolor{la_dark}{RGB}{59,54,81}     %#3B3651\n",
    "\\definecolor{la_gray}{RGB}{96,112,139}   %#60708B\n",
    "\\definecolor{la_tan}{RGB}{152,159,122}   %#989F7A\n",
    "\n",
    "\\def\\xa{1.4}\n",
    "\\def\\xb{2.1}\n",
    "\\def\\aax{1.9}\n",
    "\\def\\abx{1}\n",
    "\\def\\aby{1}\n",
    "\n",
    "\\coordinate (O)  at (0,0);\n",
    "\\coordinate (AA) at (\\aax,0);\n",
    "\\coordinate (AB) at (\\abx,\\aby);\n",
    "\n",
    "\\coordinate (AAX) at ({\\xa*\\aax},0);\n",
    "\\coordinate (ABX) at ({\\xb*\\abx},{\\xb*\\aby});\n",
    "\\coordinate (S)   at ({\\xb*\\abx+\\aax},{\\xb*\\aby});\n",
    "\\coordinate (SX)   at ({\\abx+\\aax},{\\aby});\n",
    "\n",
    "\\coordinate (B) at ({\\xa*\\aax+\\xb*\\abx},{\\xb*\\aby});\n",
    "\\coordinate (D) at ({\\xa*\\aax+\\xb*\\abx+\\aax},{\\xb*\\aby});\n",
    "\n",
    "\\draw [densely dotted,black] (ABX) -- (D) ;\n",
    "\\draw [densely dotted,black] (AAX) -- (B) ;\n",
    "\\draw [->,-latex,thick,black] (O) -- (ABX) node [above,pos=1]{$x_2 a_2$};\n",
    "\\draw [->,-latex,thick,black] (O) -- (AAX) node [right,pos=1]{$x_1 a_1$};\n",
    "\\draw [->,-latex,thick,black] (O) -- (AA) node [below,pos=.5]{$a_1$};\n",
    "\\draw [->,-latex,thick,black] (O) -- (AB) node [left,pos=1]{$a_2$};\n",
    "\\draw [->,-latex,thick,red] (O) -- (B) node [above,pos=1]{$b$};\n",
    "\\draw [la_tan] (AA) -- (D) ;\n",
    "\\draw [la_tan] (B) -- (D) ;\n",
    "\\draw [la_dark] (AA) -- (S) ;\n",
    "\\draw [la_dark] (ABX) -- (S) ;\n",
    "\\draw [la_dark] (AB) -- (SX) ;\n",
    "\n",
    "\\fill [la_dark,opacity=0.3] (O) -- (AA) -- (S) -- (ABX) -- cycle;\n",
    "\\fill [la_tan,opacity=0.3] (O) -- (AA) -- (D) -- (B) -- cycle;\n",
    "-->"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1.3 General Case"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This works in general: **the solution** of $A x = b$ for an invertible matrix of size $N \\times N$ is given by<br><br>\n",
    "$\n",
    "\\qquad x_i \\;=\\; \\frac{\\left| a_1 \\dots a_{i-1} \\; \\color{red}b \\; a_{i+1} \\dots a_N \\right|}{\\left| A \\right|}\n",
    "$\n",
    "$\\quad$ the $i^{th}$ column of $A$ is replaced with $b$, $i = 1,2, \\dots N$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Example**\n",
    ">\n",
    "> Let $\\; A = \\left( \\begin{array}{rrr} 1 &  1 &  2 \\\\\n",
    "                      -1 &  1 &  3 \\\\\n",
    "                       1 &  0 &  1  \\end{array} \\right), \\quad$\n",
    "                       $b = \\begin{pmatrix}3\\\\3\\\\3 \\end{pmatrix},$\n",
    "$\\quad$\n",
    "then $\\; \\color{red}{x_3} = \\frac{ \\left| \\begin{array}{rrr} 1 &  1 &  \\color{red}3 \\\\\n",
    "                      -1 &  1 &  \\color{red}3 \\\\\n",
    "                       1 &  0 & \\color{red}3  \\end{array} \\right|}{ \\left| A \\right|} \\;=\\; \\frac{6}{3} \\;=\\; 2\n",
    "                       $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.2 A Formula for the Inverse"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;padding-left:1cm;height:6.3cm;width:52%;\">\n",
    "We can use Cramer's Rule to obtain a Formula for the Inverse of a Matrix:\n",
    "\n",
    "* Recall that **the $\\mathbf{i^{th}}$ column of $\\mathbf{A^{-1}}$ is the solution of $\\mathbf{A x = e_i}$**,<br>\n",
    "$\\quad$ where $e_i$ is the $i^{th}$ column of the identity matrix\n",
    "\n",
    "* Substituting $b = e_j$ for column $i$ of $A$ yields the cofactor $C_{j i}$\n",
    "<br><br>\n",
    "$\\qquad A^{-1} = \\frac{1}{\\left| A \\right| } \\left( C_{i j} \\right)^t$\n",
    "</div>\n",
    "<div style=\"float:left;width:38%;border-left:2px solid black;padding-left:1cm;\">\n",
    "<strong>Example:</strong><br>\n",
    "$A = \\begin{pmatrix} a & b \\\\ c & d \\end{pmatrix}$<br><br>\n",
    "\n",
    "The cofactors are<br>$\\quad C_{1 1} = d,\\; C_{1 2} = -c,\\; C_{2 1} = -b, \\; C_{2 2} = a$<br><br>\n",
    "$\\quad A^{-1} = \\frac{1}{\\left| A \\right|} \\begin{pmatrix} d & -c \\\\ -b & a \\end{pmatrix}^t$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark:**<br>\n",
    "$\\quad$ to obtain the inverse using Cramer's rule is **not practical.** It requires\n",
    "* the determinant of $A$ of size $N \\times N$\n",
    "* $N^2$ determinants of size $(N-1) \\times (N-1)$ for each of the cofactors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Example: Generating Inverses with Integer Entries**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This does not mean that knowing this formula is not useful.\n",
    "\n",
    "**Generating examples for the computation of $A^{-1}$ that only requires integers** can be done\n",
    "* by noticing that cofactors of a matrix with integer entries are integers\n",
    "* fractions can be avoided by constructing a matrix with $\\left| A \\right| = 1$\n",
    "\n",
    "<hr>\n",
    "So, let $A = U V$, where\n",
    "\n",
    "*  $U$ is unit lower triangular with integer entries\n",
    "*  $V$ is unit upper triangular with integer entries"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Example:**\n",
    ">\n",
    "> $$A = \\left( \\begin{array}{rrr} \\color{red}1&0&0 \\\\ -2& \\color{red}1&0 \\\\ 1&1& \\color{red}1\\end{array} \\right)\n",
    " \\left( \\begin{array}{rrr} \\color{red}1&3&1 \\\\ 0& \\color{red}1&2 \\\\ 0&0& \\color{red}1\\end{array} \\right)\n",
    "\\;=\\; \\left( \\begin{array}{rrr} 1&3&1 \\\\ -2&-5&0 \\\\ 1&4& 4\\end{array} \\right)\n",
    "\\;\\Leftrightarrow\\; A^{-1} =\n",
    " \\left( \\begin{array}{rrr} -20& -8& 5 \\\\ 8& 3 & -2 \\\\ -3 & -1 & 1\\end{array} \\right)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. Take Away"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* A practical algorithm to compute the determinant:<br>\n",
    "$\\qquad$ **use Gaussian Elimination**\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Useful formulae<br>\n",
    "    * $\\left| A B \\right| = \\left| A \\right| \\left| B \\right|$\n",
    "    * $\\left| A ^k\\right| \\;\\;= \\left| A \\right|^k\\;$\n",
    "    $\\qquad$ (the integer $k$ can be negative provided $A^{-1}$ exists)\n",
    "    * $\\left| \\alpha A \\right| \\;= \\alpha^{\\color{red}N} \\left| A \\right| ,\\quad$ (here $N \\times N$ is the size of $A$)\n",
    "    * $\\left| A^t \\right| \\;\\;\\ = \\left| A \\right|$\n",
    "<br><br>\n",
    "    * $A^{-1}$ exists iff $\\left| A \\right| \\ne 0$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* A new way of solving $A x = b$ by transforming the set of equations<br>\n",
    "    * we used the wedge product, and obtained **Cramer's Rule**\n",
    "    * and applied it to obtain a solution for $A^{-1}$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
