{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align: center;\"><strong style=\"height:100px;color:darkred;font-size:40px;\">Appendix 1: Analytic Geometry</strong></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 1. Vectors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Geometric statements can be translated into algebraic statements (and vice versa) by introducing **coordinate systems**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **The common concepts are vectors:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;\">\n",
    "\n",
    "* a directed line segment connecting a starting point $A$ to an end point $B$ written as $AB$\n",
    "\n",
    "$\\quad$ Two properties we will use are\n",
    "* reversing the direction of a vector introduces a minus sign: $\\quad AB = - BA$\n",
    "* Vector additition allows us to take a detour via a point C: $\\quad AB = AC + CB$<br>\n",
    "    (insert the detour point $C$ in between $AB$)\n",
    "</div>\n",
    "<div style=\"float:left;margin-left:3.5cm;\"><img src=\"Figs/a1_vector_addition.svg\" width=350></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Vectors with starting point at the origin $O$**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;\">\n",
    "\n",
    "* $AB = AO + OB = OB - OA \\quad$   (\"end point minus starting point\")\n",
    "* Vector components in 2D:<br>\n",
    "$\\quad$ The invertible transformation $\\mathscr{D}$ takes a vector and represents it by its coordinates<br>$\\qquad$ with respect to a set of **coordinate vectors** typically called $\\mathbf{i}$ and $\\mathbf{j},$<br>\n",
    "$\\qquad\\qquad$ $\\mathscr{D}(x \\mathbf{i} + y \\mathbf{j} ) =  \\begin{pmatrix} x \\\\ y \\end{pmatrix}$<br><br>\n",
    "    \n",
    "This is how we translate from geometric vectors to algebraic expressions\n",
    "</div>\n",
    "<div style=\"float:left;margin-left:3cm;\"><img src=\"Figs/a1_polar_coordinates.svg\" width=400></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Finding coordinate vectors from geometric descriptions is often accomplished by making use of **polar coordinates**<br>\n",
    "$\\qquad\\qquad \\left\\{ \\;\\; \\begin{align} x =& \\; r\\; cos \\theta \\\\ y = & \\; r\\; sin \\theta \\end{align} \\right.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* A useful tool to find lengths of vectors and angles between vectors is to use **congruent triangles**\n",
    "    * three sides are equal\n",
    "    * two sides and an angle are equal\n",
    "    * one side and 2 angles are equal"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 2. Formulae from Trigonometry"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 2.1 Euler's Formula"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "    <strong>Theorem:</strong><br>\n",
    "$\\quad$ Given $\\theta \\in \\mathbb{R}$ we have $e^{i \\theta} = cos \\theta + i \\ sin \\theta$\n",
    "<div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The formula is readily generalized to complex numbers $z = x + i \\ y$. Properties we will use are\n",
    "$$\\begin{align}\n",
    "    e^{x + i y} =&\\ e^x e^{ i y} = e^x \\left( cos y + i\\ sin y \\right) & \\qquad & \\text{for any } x,y \\in \\mathbb{R}\\\\\n",
    "    e^{z+w} =&\\ e^z + e^w & \\qquad & \\text{for any } z,w \\in \\mathbb{C}\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks:**\n",
    "* If we set $r = e^x, \\ \\theta = y,$ we see that $e^{x+i\\ y} =r e^{i\\ \\theta}$ can be interpreted as a representation of a point in polar coordinates."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2 Formulae based on Euler's Formula and Complex Multiplication"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Special Cases:**<br>\n",
    "<div style=\"float:left;padding-right:1cm;height:4.0cm;\">\n",
    "$$\\begin{align}\n",
    "e^{0} =&\\ 1 \\\\\n",
    "e^{\\frac{\\pi}{2}i} =&\\ \\;\\;\\; i \\;\\;( 90^\\circ ) \\\\\n",
    "e^{\\pi i} =&\\ \\; -1 \\;\\;( 180^\\circ ) \\\\\n",
    "e^{-\\frac{\\pi}{2}i} =&\\ - i \\;\\;( 90^\\circ ) \\\\\n",
    "\\end{align}$$\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:1cm;padding-right:1cm;border-left:2px solid black;height:4.0cm;\">\n",
    "$$\\begin{align}\n",
    "\\color{red}{cos\\ ( \\theta + \\pi )} + i \\color{blue}{\\ sin ( \\theta + \\pi )}\n",
    "    =&\\ e^{i\\ (\\theta + \\pi )} \\\\\n",
    "    =&\\ e^{i \\ \\theta} e^{i \\ \\pi} \\\\\n",
    "    =&\\ \\left( cos \\ \\theta + i \\ sin\\ \\theta \\right) \\left( -i \\right) \\\\\n",
    "    =&\\ \\color{red}{ sin \\ \\theta } - i \\ \\color{blue}{ cos\\ \\theta } \\\\\n",
    "\\end{align}$$\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:1cm;border-left:2px solid black;border-left:2px solid black;height:4.0cm;\">\n",
    "\n",
    "Since $120^\\circ = 90^\\circ + 30^\\circ$, we have\n",
    "\n",
    "$$\\begin{align}\n",
    "\\color{red}{cos \\ 120^\\circ} + i \\color{blue}{\\ sin\\ 120^\\circ} =&\\  e^{\\frac{\\pi}{2}i}\\; e^{\\frac{\\pi}{3}i}\\\\\n",
    "                                     =&\\ i\\ e^{\\frac{\\pi}{3} i} \\\\\n",
    "                                     =&\\ \\color{red}{-\\frac{1}{2}} +i \\color{blue}{\\ \\frac{\\sqrt{3}}{2}}\n",
    "\\end{align}$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "Cosine and sine values for the angles $0^\\circ, 30^\\circ, 45^\\circ, 60^\\circ$ and $90^\\circ$ in increasing order are easy to remember.\n",
    "\n",
    "<div style=\"float:left;padding-left:1cm;padding-right:1cm;border-right:2px solid black;\">\n",
    "    Here is a <strong>mnemnonic:</strong>\n",
    "\n",
    "| angle $\\theta$ | $0^\\circ$ | $30^\\circ$ | $45^\\circ$ | $60^\\circ$ | $90^\\circ$ |\n",
    "| :---- | --- | --- | ---- | ---- | ---- |\n",
    "| sin$\\ \\theta$   |  $\\tfrac{\\sqrt{\\color{red}0}}{2}$ |$\\tfrac{\\sqrt{\\color{red}1}}{2}$ | $\\tfrac{\\sqrt{\\color{red}3}}{2}$ | $\\tfrac{\\sqrt{\\color{red}3}}{2}$| $\\tfrac{\\sqrt{\\color{red}4}}{2}$|\n",
    "| cos$\\ \\theta$   |  $\\tfrac{\\sqrt{\\color{red}4}}{2}$ |$\\tfrac{\\sqrt{\\color{red}3}}{2}$ | $\\tfrac{\\sqrt{\\color{red}2}}{2}$ | $\\tfrac{\\sqrt{\\color{red}1}}{2}$| $\\tfrac{\\sqrt{\\color{red}0}}{2}$|\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:1cm;\">\n",
    "Addition of angles is easily handled by the formulae for the exponentials and complex multiplication:\n",
    "\n",
    "$$\\begin{align}\n",
    "\\color{red}{cos\\ ( \\theta + \\phi )} + i \\color{blue}{\\ sin ( \\theta + \\phi )}\n",
    "    =&\\ e^{i\\ (\\theta + \\phi )} \\\\\n",
    "    =&\\ e^{i \\ \\theta} e^{i \\ \\phi} \\\\\n",
    "    =&\\ \\left( cos \\ \\theta + i \\ sin\\ \\theta \\right) \\left( cos \\ \\phi + i \\ sin\\ \\phi \\right) \\\\\n",
    "    =&\\ \\color{red}{ cos \\ \\theta\\ cos \\ \\phi - sin\\ \\theta\\ sin\\ \\phi}\n",
    "      +  i \\ \\color{blue}{\\left( cos \\ \\theta\\ sin \\ \\phi + sin\\ \\theta\\ cos\\ \\phi \\right) } \\\\\n",
    "\\end{align}$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### The Unit Circle"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;\">\n",
    "Points on the unit circle have coordinates<br>\n",
    "$ \\begin{align}x =&\\ cos\\ \\theta \\\\ y=&\\ sin\\ \\theta \\end{align}$<br><br>\n",
    "\n",
    "The coordinates of angles $\\phi = n \\frac{\\pi}{2}$<br>\n",
    "    $\\quad$ can be read out using congruent triangles.\n",
    "    \n",
    "E.g., $x = cos\\ 120^\\circ = - sin\\ 30^\\circ = -\\frac{1}{2}$\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:3cm;\">\n",
    "<img src=\"Figs/a1_unit_circle.svg\" width=350>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 3. Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### 3.1 Reflection With Respect To a Line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the reflection of a point $A$ with respect to a a line $y = x\\; \\tan \\phi$, as shown in the following diagram:\n",
    "\n",
    "<div style=\"float:left;\">\n",
    "<img src=\"./Figs/LinTxSymLine.svg\"  width=\"200\">\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:3cm;\">\n",
    "\n",
    "<br>\n",
    "\n",
    "Observe that the triangles OCA and OCB are congruent:\n",
    "* The angles $\\angle OCB = \\angle OCA = 90^\\circ$\n",
    "* The side $OC$ is in common\n",
    "* The side $AC = CB$ by construction\n",
    "\n",
    "Given a point $A$ at $(r \\cos \\theta, r \\sin \\theta)$, we see that<br>$\\quad$ the reflected point $B$ has coordinates\n",
    "$\\left(r \\cos ( 2 \\phi - \\theta ), r \\sin ( 2 \\phi- \\theta) \\right)$\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This problem is simple enough that we can write down the solution directly:\n",
    "* start with the point $A$ with polar coordinates $\\left[ r, \\theta \\right]$\n",
    "* draw the orthogonal reflection $B$ through a line $(L)$ that makes an angle $\\phi$ with respect to the x axis\n",
    "* show the triangles $OCA$ and $OCB$ are congruent and derive the angles\n",
    "* the point $B$ is at a distance $r$ from the origin and an angle $\\phi+(\\phi - \\theta) = 2\\phi - \\theta$ with respect to the x axis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Therefore, expanding using trigonometric identities, we have\n",
    "\n",
    "$$\\begin{align}\n",
    "\\begin{pmatrix} x' \\\\ y' \\end{pmatrix}\n",
    "=&\\ \\begin{pmatrix} r \\cos(2 \\phi - \\theta) \\\\ r \\sin( 2 \\phi -\\theta)\\end{pmatrix}\n",
    "=  \\begin{pmatrix} r \\cos(2\\phi) \\cos(\\theta)+ r \\sin(2\\phi) \\sin(\\theta) \\\\ r \\sin(2\\phi) \\cos(\\theta)- r \\cos(2\\phi)\\sin(\\theta) \\end{pmatrix}\n",
    "= \\begin{pmatrix} \\cos( 2 \\phi) & \\sin( 2 \\phi) \\\\ \\sin(2\\phi) & - \\cos(2\\phi) \\end{pmatrix}\n",
    "  \\begin{pmatrix} r \\cos(\\theta) \\\\ r \\sin(\\theta) \\end{pmatrix}\\\\[2mm]\n",
    "=&\\ \\begin{pmatrix} \\cos( 2 \\phi) & \\sin( 2 \\phi) \\\\ \\sin(2\\phi) & - \\cos(2\\phi) \\end{pmatrix}\n",
    "  \\begin{pmatrix} x \\\\ y \\end{pmatrix}\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark:**<br>\n",
    "$\\quad$ We found that coordinates of $B$ are obtained from the coordinates of $A$ by multiplication with a matrix<br>\n",
    "$\\quad$ Since this matrix does not depend on the coordinates of $A$, the orthogonal reflection through a line is a **linear transformation.** "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### **Example: the Mirror Image of a Point with Respect to the Line $y = x\\ tan\\ 30^\\circ$**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The line makes an angle of $30^\\circ$ degrees with respect to the x axis.<br>\n",
    "\n",
    "Substituting $cos 60^\\circ = \\frac{1}{2}, sin 60^\\circ = \\frac{\\sqrt{3}}{2}$ into the result above, we obtain\n",
    "\n",
    "$$\n",
    "\\begin{pmatrix} x' \\\\ y' \\end{pmatrix}\n",
    "=\\ \\frac{1}{2} \\begin{pmatrix} 1 & \\sqrt{3} \\\\ \\sqrt{3} & -1 \\end{pmatrix} \\begin{pmatrix}  x \\\\ y \\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 3.2 Orthogonal Projection Onto a Line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The problem is again simple enough so we can write the solution directly: we observe\n",
    "\n",
    "$$OC = OA + \\frac{1}{2} AB = OA + \\frac{1}{2} \\left( OB - OA \\right)$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Therefore, using the reflection result above\n",
    "\n",
    "$$\\begin{align}\n",
    "\\begin{pmatrix}x' \\\\ y' \\end{pmatrix} =&\\\n",
    "\\begin{pmatrix}x \\\\ y \\end{pmatrix}\n",
    "+ \\frac{1}{2} \\left( \\;\n",
    " \\begin{pmatrix} \\cos (2\\phi) & \\sin (2\\phi) \\\\ \\sin (2\\phi) & -\\cos (2\\phi) \\end{pmatrix} \\begin{pmatrix} x \\\\ y \\end{pmatrix}\n",
    "- \\begin{pmatrix} x \\\\ y \\end{pmatrix}\n",
    "\\;\\right) \\\\[1mm]\n",
    "=&\\ \\frac{1}{2} \\begin{pmatrix} 1+\\cos ( 2\\phi) & \\sin ( 2\\phi)\\\\ \\sin (2\\phi) & 1 - \\cos ( 2\\phi)\\end{pmatrix}\\begin{pmatrix} x \\\\ y \\end{pmatrix} \\\\[1mm]\n",
    "=& \\\n",
    "\\begin{pmatrix} \\cos^2 \\phi & \\cos \\phi \\sin\\phi \\\\ \\cos\\phi \\sin\\phi & \\sin^2 \\phi \\end{pmatrix}\n",
    "\\begin{pmatrix}x \\\\ y \\end{pmatrix}\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Example: The Orthogonal Projection of a Point onto the Line $y = x\\ tan\\ 30^\\circ$**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The line makes an angle of $30^\\circ$ degrees with respect to the x axis.<br>\n",
    "\n",
    "Substituting $cos 30^\\circ = \\frac{\\sqrt{3}}{2}, sin 30^\\circ = \\frac{1}{2}$ into the result above, we obtain\n",
    "\n",
    "$$\n",
    "\\begin{pmatrix} x' \\\\ y' \\end{pmatrix}\n",
    "=\\ \\frac{1}{4} \\begin{pmatrix} 3 & \\sqrt{3} \\\\ \\sqrt{3} & 1 \\end{pmatrix} \\begin{pmatrix}  x \\\\ y \\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. Take Away"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Translating geometric statements to algebraic statements using vectors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Euler's Formula and Congruent Triangles\n",
    "    * obtain basic trigonometric identities\n",
    "    * the values of sine and cosine for some angles"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Congruent Triangles\n",
    "    * the values of sine and cosine for some angles using congruent triangles on the unit circle"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Application\n",
    "    * the reflection of a point through a line (the mirror image)\n",
    "    * the orthogonal projection of a point onto a line"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.4",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
