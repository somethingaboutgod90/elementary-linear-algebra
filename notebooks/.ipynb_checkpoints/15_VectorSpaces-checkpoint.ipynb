{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "using LinearAlgebra, Plots, LaTeXStrings\n",
    "include(\"LAcodes.jl\")\n",
    "using PyCall\n",
    "itikz=pyimport(\"itikz\")\n",
    "jinja=pyimport(\"jinja2\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align:center;\"><strong style=\"height:100px;color:darkred;font-size:40px;\">Vector Spaces</strong>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "html\"<iframe width=\\\"400\\\" height=\\\"200\\\" src=\\\"https://www.youtube.com/embed/dWQAH4izUTo\\\"  frameborder=\\\"0\\\" allow=\\\"accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture\\\" allowfullscreen></iframe>\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Linear Combinations of Vectors, Vector Spaces"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1 Motivation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "There are many mathematical objects that obey the rules of linear combinations:\n",
    "\n",
    "**Example:** Functions  $$f(x) = 5 + 3 \\sin x + 2 \\cos x$$\n",
    "\n",
    "is a linear combination of the functions $f_1(x) = 1, f_2(x) = \\ x, f_3(x) = \\cos x$:\n",
    "\n",
    "$$f(x) = 5 f_1(x) + 3 f_2(x) + 2 f_3(x).$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "> We will generalize the notion of vector to any such objects!\n",
    "> \n",
    "> The basic ingredients are\n",
    "> * a set of **vectors**\n",
    "> * scalars (we will use $\\mathbb{R}$, though most of our examples use $\\mathbb{Q})$.\n",
    "> * addition and scalar multiplication operations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2 Definition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2.1 Definition of a Vector Space"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:** A **vector space** consists of\n",
    "* a set of vectors $V$\n",
    "* a set of scalars $\\mathbb{R}$\n",
    "* an addition operation $+ : V \\times V \\longrightarrow V$\n",
    "* a scalar multiplication operation  $\\cdot : \\mathbb{R} \\times V \\longrightarrow V$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "For any vectors $u, v$ and $w$ in V, and any scalars $\\alpha, \\beta$ in $\\mathbb{R}$,\n",
    "the following axioms hold:\n",
    "\n",
    "1. $u+v = v+u$\n",
    "2. $u+(v+w) = (u+v)+w$\n",
    "3. $\\exists\\ 0 \\in V$ such that $u + 0 = u$\n",
    "4. $\\exists\\ \\tilde{u} \\in V$ such that $u + \\tilde{u} = 0$\n",
    "    \n",
    "5. $\\alpha ( u + v ) = \\alpha u +\\alpha v$\n",
    "6. $( \\alpha + \\beta) u = (\\alpha u) + (\\beta u)$\n",
    "7. $\\alpha ( \\beta u ) = ( \\alpha \\beta ) u$\n",
    "8. $1 u = u$\n",
    "</div>\n",
    "\n",
    "**Remark:** When the scalars and the operations are known from the context, it is customary to refer to the vector space by its set of vectors: $V$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "----\n",
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Theorem:**\n",
    "* The 0 vector is unique.<br>\n",
    "  For any vector $v \\in V$, $\\color{red}{\\text{the zero vector} \\quad 0} = 0\\ v$\n",
    "* The additive inverse $\\tilde{u}$ is unique.<br>\n",
    "  For any vector $v \\in V$, $\\quad \\tilde{v} = -1 v$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "**Remarks:**\n",
    "* The additive inverse $\\quad \\tilde{v} = -1 v  \\quad$ will be written $\\quad -v$.\n",
    "* The linear combination $\\; u + (-1 v) \\quad$ will be written $\\quad u - v$\n",
    "* Equality between vectors has the properties\n",
    "    * $u = v \\Leftrightarrow u+w = v+w$\n",
    "    * $u = v \\Leftrightarrow \\alpha u = \\alpha v, \\alpha \\ne 0$\n",
    "\n",
    "**The algebra of linear combinations works similarly to scalar algebra**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2.2 Example of a Computation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $u, v$ and $x$ be vectors in a vector space $V$\n",
    "$$\n",
    "\\begin{align}\n",
    "    (\\xi) & \\Leftrightarrow 3 x + 2 u - v = 5 x - u + v & \\quad &\\\\\n",
    "          & \\Leftrightarrow 3 x - 5 x = -2 u + v - u + v & \\quad & \\text{ using the properties of equality and vector addition}\\\\\n",
    "          & \\Leftrightarrow -2 x = - 3 u + 2 v           & \\quad & \\text{ using Axioms 1,2 and 6}\\\\\n",
    "          & \\Leftrightarrow x = \\frac{3}{2} u - v          & \\quad & \\text{ using the properties of equality and scalar multiplication}\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "**Remark:** We have *not* specified what the \"vectors\" are in this computation. We only said they were vectors in a  vector space!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.3 Examples and Further Definitions and Theorems"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3.1 Vectors in $\\mathbb{R}^N$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> The vectors in $\\mathbb{R}^N$ satisfy all of the axioms of a vector space,<br>\n",
    "and therefore **$\\mathbb{R}^N$ is a vector space.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.3.1.1 The Vector Space $V = \\{ 0 \\}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The zero vector in any vector space satisfies all of the axioms of a vector space.\n",
    "\n",
    "In particular, $V = \\left\\{ \\; \\begin{pmatrix}0\\\\0\\\\0 \\end{pmatrix}\\; \\right\\}$,\n",
    "i.e., **the set of vectors consisting of just the origin in $\\mathbb{R}^3$\n",
    "is a vector space.**\n",
    "\n",
    "**Remark:** $V \\subset \\mathbb{R}^3$:  this is a vector space that is contained in a vector space!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.3.1.2 Vectors in a Span of Vectors in $\\mathbb{R}^3$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $v_1 = \\begin{pmatrix} 3 & -1 & 1 \\end{pmatrix}$ and $v_2 = \\begin{pmatrix} 1 & -1 & 1 \\end{pmatrix}$ be two vectors in $\\mathbb{R}^3$.\n",
    "\n",
    "**The plane $P = span \\{ v_1, v_2 \\}$ through the origin satisfies all of the axioms of a vector space.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the definition of a span to rewrite\n",
    "$$\n",
    "P = \\left\\{ v \\ \\bigg| \\ v = \\alpha v_1 + \\beta v_2 \\text{ for any } \\alpha, \\beta \\text{ in } \\mathbb{R}    \\right\\}\n",
    "$$\n",
    "The reader is encouraged to check each of the properties of addition, scalar multiplication, and each of the axioms of a vector space."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> It turns out that the most important properties are\n",
    "> * **For any two vectors $v_1, v_2$ in $V$, their sum $v_1 + v_2$ is in $V$:**<br>\n",
    "Take any two vectors in the plane, and note that their sum is a vector in the plane.\n",
    "> * **For any vector $v$ in $V$, and any scalar $\\alpha$ in $\\mathbb{R}$, the scalar product $\\alpha v$ is in $V$.**<br>\n",
    "Take any vector in the plane and multiply it by some scalar: the resultant vector is in the plane.\n",
    "\n",
    "**Remark:** We can rephrase this as **any linear combination of vectors in $V$ is a vector in $V.$**<br>No matter which vectors we pick in $V$, and what linear combinations of these vectors we form, we cannot escape from $V$!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark:** The <font style=\"color:blue;\">plane</font> $P$ in this example is a vector space contained in $\\mathbb{R}^3$.<br>\n",
    "<font style=\"color:blue;\">Any one vector in this plane, e.g., the zero vector, has *three entries!*</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3.2 Closed under Addition, Closed under Scalar Multiplication, Spans"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:** Let $S$ be a set of vectors in a vector space.<br>\n",
    "* $S$ is **closed under addition** iff the sum of any two vectors $s_1, s_2$ in $S$ is also in $S$.\n",
    "* $S$ is **closed under scalar multiplication** iff the scalar product of any vectors $s$ in $S$ with any scalar is also in $S$.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Theorem:** Let $S = \\left\\{ s_1, s_2, \\dots s_k \\right\\}$ be a set of vectors in a vector space $V$.<br>\n",
    "The $span\\left\\{ S \\right\\}$ is a vector space contained in $V$\n",
    "</div>\n",
    "\n",
    "This definition and theorem apply to any vector space, not just $\\mathbb{R}^N$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3.3 Vector Spaces of Matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.3.3.1 The Set of Matrices of the Same Size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $\\mathscr{M}_{2,3} = \\left\\{ A  \\ \\bigg| \\  A \\text{ is a matrix of size } 2 \\times 3 \\right\\}$\n",
    "\n",
    "This set is closed under addition and scalar multiplication. All of the axioms of a vector space hold:<br> **$\\mathscr{M}_{2,3}$ is a vector space.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.3.3.2 A Subset of a Set of Matrices of the Same Size "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $S = \\left\\{ A  \\ \\bigg| \\ A \\text{  is an upper triangular matrix of size } 3 \\times 3 \\right\\} \\subset \\mathscr{M}_{3,3}$.\n",
    "\n",
    "We can again check closure under addition and scalar multiplication, and all the axioms:<br>\n",
    "**$S$ is a vector space contained in the vector space $\\mathscr{M}_{3,3}.$**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3.4 Vector Spaces of Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.3.4.1 The Vector Space of Functions $\\mathscr{F}(-\\infty,\\infty)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**The set of functions\n",
    "$\\mathscr{F}(-\\infty,\\infty) = \\left\\{ f(x)  \\ \\bigg| \\  f : \\mathbb{R} \\longrightarrow  \\mathbb{R} \\right\\}$\n",
    "is a vector space.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Think of vectors $v$ in $\\mathbb{R}^N$ as functions from the index $i = 1, 2, \\dots N$ to the corresponding entry $v_i$.<br>\n",
    "A function generalizes this notion: it is a vector with indices $x \\in \\mathbb{R}$ that maps\n",
    "an index $x$ to a corresponding value $f(x)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "x = [-3,-2,-1,0,1,3,4]\n",
    "f = x.^2 .+ 1\n",
    "println(\"Vector f = $f\")\n",
    "scatter( x, f, line=:stem, marker=:circle, color=:blue, markersize=5, label = L\"The\\ vector\\ f\", legend=:top)\n",
    "\n",
    "x=range(-5,stop=5, length=50)\n",
    "plot!( x, x.^2 .+ 1, label=L\"The\\ function\\ x^2+1\" )\n",
    "plot!(size=(500,250), ylims=(0,20))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.3.4.2 The set of Polynomials $\\mathscr{P}_2[-1,1]$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the set of polynomials of degree less than or equal to 2, with $-1 \\le x \\le 1$\n",
    "$$\n",
    "\\begin{align}\n",
    "\\mathscr{P}_2[-1,1] & = \\left\\{ p(x) \\ \\bigg| \\  p(x) = \\alpha + \\beta x + \\gamma x^2, \n",
    "\\quad\\text{for any } \\alpha,\\beta,\\gamma \\text{ in } \\mathbb{R} \\right\\} \\\\\n",
    "& = span \\left\\{\\ p_1(x) = 1, p_2(x) = x, p_3(x) = x^2 \\ \\right\\}\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "This is **a vector space contained in the vector space of functions $\\mathscr{F}[-1,1]$.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "x = range(-1, stop=1, length=50)\n",
    "plot( x, 1 .- 3x.^2, label = L\"p(x) = 1 - 3 x^2\", legend=:topleft)\n",
    "plot!( x, -1 .+ 3x.^2, label = L\"\\tilde{p}(x) = -1 + 3 x^2\")\n",
    "plot!( x, 0*x, label=L\"zero\\ vector\\ z(x) = 0\" )\n",
    "plot!(size=(500,250), ylims=(-3,5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark:** Note the zero vector! It is a function $z(x)$.<br>$\\quad$ To get it, take any function, e.g., $f(x) = x$, and multiply it by zero: $z(x) = 0$, the x axis of our plot!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3.5 Set Notation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The set notation used in the examples looks as follows:\n",
    "$$\n",
    "\\color{red}{\\text{< Name of the set >}} = \\left\\{\n",
    "\\color{magenta}{\\text{<vector>}}  \\ \\bigg| \\  \\color{green}{\\text{< membership constraint(s) >}}\n",
    " \\right\\}\n",
    "$$\n",
    "\n",
    "For example,\n",
    "$$\\color{red}{S} = \\left\\{\n",
    "\\color{magenta}{\\begin{pmatrix} x \\\\ y \\end{pmatrix}}\n",
    "\\;  \\ \\bigg| \\  \\quad \\color{green}{-1 < x < 1, \\text{and } y \\ge 0}\n",
    "\\right\\} \\subset \\mathbb{R}^2\n",
    "$$\n",
    "\n",
    "defines a set named $S$, consisting of vectors $\\begin{pmatrix} x\\\\y \\end{pmatrix}$ (actual vectors in $\\mathbb{R}^2$).<br>The vectors belong to the set $S$ iff the membership constraints $-1 < x < 1$ and $y \\ge 0$ are satisfied.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Subspaces of a Vector Space"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.1. Definition, Subspace Test"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we have seen, vector spaces can contain vector spaces. This motivates the following\n",
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:** Let $V$ be a vector space. A set of vectors $S$ is a **subspace** of $V$ iff\n",
    "* $S \\subseteq V$\n",
    "* $S$ is not empty, i.e. $S \\ne \\emptyset$\n",
    "* $S$ is a vector space with the same scalars, vector addition and scalar multiplication as $V$.\n",
    "<div>\n",
    "\n",
    "**Remark:** A set is empty if it contains no element. The set $\\left\\{ 0 \\right\\}$ is *not empty*: it contains the zero vector."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may have noticed that checking whether a set $S$ is a subset of a vector space $V$\n",
    "does not require checking all the axioms: most of them are inherited from $V$!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Theorem:** Let $V$ be a vector space. $S$ is a **subspace** of $V$ iff\n",
    "1. $S$ is not empty\n",
    "2. $S$ is closed under vector addition\n",
    "3. $S$ is closed under scalar multiplication\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark:** Since vector spaces must contain the zero vector,\n",
    "it is often advantageous to check for property 1. of a subspace by verifying that\n",
    "**the zero vector is in the set** $S$, i.e.,\n",
    "we replace property 1. in the above definition with\n",
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "1. The zero vector $0 \\in S$.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "**Remarks:**\n",
    "* Checking whether a set $S$ is a subspace of $V$ requires checking that each of the properties 1., 2. and 3. holds.<br> If any one of these fails, $S$ is not a subspace.\n",
    "* We start with the conjecture that $S$ is a subspace.\n",
    "    * examples that show that the sum of two example vectors in $S$ is also in $S$ is not sufficient: one would have to verify all possible combinations of all possible vectors.<br>No \"Proof\" by example!\n",
    "    * a counter example that shows our conjecture is false hovever disproves the conjecture:<br>This is **proof by contradiction**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "> **Template of a proof:** substitute the appropriate text for < ... >\n",
    "> \n",
    "> *Step 1:* Identify the $\\color{magenta}{\\text{< vectors >}}$,\n",
    "> and the $\\color{green}{\\text{< membership constraint(s) >}}.$<br>\n",
    "> Identify the zero vector $< \\color{magenta}{z} >$.\n",
    "> \n",
    "> *Step 2:* Check each of the properties 1, 2 and 3 in any order (If you notice a counterexample > for any one of these,you are done!)\n",
    "> \n",
    "> 1. The zero vector is $< \\color{magenta}{z} >$.<br>\n",
    "> $< \\color{magenta}{z} >$ <is/is not> in $S$ since it <does/does not> satisfy the $\\color{green}{\\text{< membership constraint(s) >}}.$\n",
    "> 2. Let $< \\color{magenta}{s_1} > \\text{ be any vector in } S \\Rightarrow \\color{green}{\\text{< membership constraint(s) >}} \\text{ holds for < } \\color{magenta}{s_1} >$.<br>\n",
    "> Let $< \\color{magenta}{s_2} > \\text{ be any vector in } S \\Rightarrow \\color{green}{\\text{< membership constraint(s) >}} \\text{ holds for } < \\color{magenta}{s_2} >$.<br>\n",
    "> Does $< \\color{magenta}{s_1 + s_2} >$ satisfy the $\\color{green}{\\text{< membership constraint(s) >}}?$\n",
    "> 2. Let $< \\color{magenta}{s_1} > \\text{ be any vector in } S \\Rightarrow \\color{green}{\\text{< membership constraint(s) >}} \\text{ holds for } < \\color{magenta}{s_1} >$.<br>\n",
    "> Let $< \\color{magenta}{\\alpha_1} > \\text{ be any scalar}$.<br>\n",
    "> Does $< \\color{magenta}{\\alpha\\ s_1} >$ satisfy the $\\color{green}{\\text{< membership constraint(s) >}}?$\n",
    "</div>\n",
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "> **Shortcut:**\n",
    "> **If the definition of $S$ can be rewritten as a span, $S$ is a subspace!**\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2 Examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2.1 Vectors in $\\mathbb{R}^N$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.2.1.1 A Span of Vectors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $S = \\left\\{ w  \\ \\bigg| \\  w=\\begin{pmatrix} s+t \\\\ s-t \\\\2 s + 3 t \\end{pmatrix}\n",
    "\\ \\text{ for any } s, t \\text{ in } \\mathbb{R} \\ \\right\\}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the membership constraint can be rewritten as a linear combination. We have\n",
    "$$\n",
    "\\begin{align}\n",
    "S & = \\left\\{ w  \\ \\bigg| \\  w=\\begin{pmatrix} s+t \\\\ s-t \\\\2 s + 3 t \\end{pmatrix}\n",
    "\\ \\text{ for any } s, t \\text{ in } \\mathbb{R} \\ \\right\\} \\\\\n",
    "& =\n",
    "\\left\\{ w  \\ \\bigg| \\  w =\n",
    "s \\begin{pmatrix} 1 \\\\ 1 \\\\ 2 \\end{pmatrix} +\n",
    "t \\begin{pmatrix}\\ 1 \\\\ -1 \\\\ \\ 3 \\end{pmatrix}\n",
    "\\ \\text{ for any } s, t \\text{ in } \\mathbb{R} \\ \\right\\} \\\\\n",
    "& =span \\left\\{\\ \\begin{pmatrix} 1 \\\\ 1 \\\\ 2 \\end{pmatrix}, \\begin{pmatrix}\\ 1 \\\\ -1 \\\\ \\ 3 \\end{pmatrix}\\ \n",
    "\\right\\}\n",
    "\\end{align},\n",
    "$$\n",
    "thus **$S$ is a subspace of $\\mathbb{R}^3$**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.2.1.2 A Set of Vectors that Do Not Contain the Zero Vector "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $S = \\left\\{ w  \\ \\bigg| \\  w=\\begin{pmatrix} s+t+1 \\\\ s-t \\\\2 s + 3 t \\end{pmatrix}\n",
    "\\ \\text{ for any } s, t \\text{ in } \\mathbb{R} \\ \\right\\}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is almost the same problem as before.\n",
    "$$\n",
    "\\begin{align}\n",
    "S & = \\left\\{ w  \\ \\bigg| \\  w=\\begin{pmatrix} s+t\\color{red}{+1} \\\\ s-t \\\\2 s + 3 t \\end{pmatrix}\n",
    "\\ \\text{ for any } s, t \\text{ in } \\mathbb{R} \\ \\right\\} \\\\\n",
    "& =\n",
    "\\left\\{ w  \\ \\bigg| \\  w =\n",
    "s \\begin{pmatrix} 1 \\\\ 1 \\\\ 2 \\end{pmatrix} +\n",
    "t \\begin{pmatrix}\\ 1 \\\\ -1 \\\\ \\ 3 \\end{pmatrix} +\n",
    "\\color{red}{ \\begin{pmatrix}\\ 1 \\\\ \\ 0 \\\\ \\ 0 \\end{pmatrix} }\n",
    "\\ \\text{ for any } s, t \\text{ in } \\mathbb{R} \\ \\right\\} \\\\\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "this is not a span. Let's try our template:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. The zero vector is $\\begin{pmatrix} 0 \\\\ 0\\\\0 \\end{pmatrix}$.<br>\n",
    "To check whether the zero vector is in $S$,we need to check whether there are values of $\\alpha,\\beta$ such that\n",
    "$$\n",
    "s \\begin{pmatrix} 1 \\\\ 1 \\\\ 2 \\end{pmatrix} +\n",
    "t \\begin{pmatrix}\\ 1 \\\\ -1 \\\\ \\ 3 \\end{pmatrix} +\n",
    "\\begin{pmatrix}\\ 1 \\\\ 0 \\\\ \\ 0 \\end{pmatrix}  = \\begin{pmatrix} 0 \\\\ 0\\\\0 \\end{pmatrix}\n",
    "\\quad \\Leftrightarrow \\quad\n",
    "\\begin{pmatrix} 1 & \\ 1 \\\\ 1 & -1 \\\\ 2 & \\ 3 \\end{pmatrix}\n",
    "\\begin{pmatrix} s \\\\ t \\end{pmatrix} = \\begin{pmatrix} -1 \\\\ 0\\\\0 \\end{pmatrix}\n",
    "$$\n",
    "Since this problem has no solution, the zero vector is not in $S$.<br>\n",
    "Therefore, **$S$ is not a subspace of $\\mathbb{R}^3$.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.2.1.3 A Set of Vectors that is Not a Span (i.e., Hyperplane)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p style=\"color:blue;font-size:15;height:15\">Sets of Vectors that are Not Subspaces</p>\n",
    "<img src=\"Figs/subspace.svg\"\n",
    "     alt=\"Sets of Vectors that are Not Subspaces\"\n",
    "     style=\"float: left; margin-right: 10px;\" width=250/>\n",
    "Consider\n",
    "$$\n",
    "\\begin{align}\n",
    "S_1 &= \\left\\{ \\begin{pmatrix} x\\\\y \\end{pmatrix}  \\ \\bigg| \\ x y \\ge 0 \\right\\}\\quad \\text{and}\\\\\n",
    "S_2 &= \\left\\{ \\begin{pmatrix} x\\\\y \\end{pmatrix}  \\ \\bigg| \\ y \\ge 0 \\right\\}\\\\\n",
    "\\end{align}\n",
    "$$\n",
    "The figure to the left shows these two sets: $S_1$ consists of quadrant 1 and 3, including the axes, while $S_2$ consists of the upper half plane including the x axis. Since neither of these are (hyper)planes, we suspect these are not subspaces."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### First, let us consider $S_1$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. The zero vector $\\begin{pmatrix} x\\\\y \\end{pmatrix} = \\begin{pmatrix} 0\\\\0 \\end{pmatrix}$\n",
    "   is in $S_1$ since $x y = 0$, satisfying the membership constraints:<br> $S_1$ **is not empty**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. Let $\\begin{pmatrix}x_1\\\\y_1\\end{pmatrix} \\in S_1 \\Rightarrow x_1 y_1 \\ge 0$,  and let $\\alpha_1$ be any scalar.<br>\n",
    "Consider $\\begin{pmatrix}\\alpha_1 x_1 \\\\ \\alpha_1 y_1 \\end{pmatrix}$. Since $\\alpha_1 x_1 \\alpha_1 y_1 = \\alpha_1^2 x_1 y_1$, and since we know $x_1 y_1 \\ge 0$, the membership criterion is satisfied: this vector is in $S_1$, and therefore<br> $S_1$ **is closed under scalar multiplication**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. Let $\\begin{pmatrix}x_1\\\\y_1\\end{pmatrix} \\in S_1 \\Rightarrow x_1 y_1 \\ge 0$,\n",
    "and let $\\begin{pmatrix}x_2\\\\y_2\\end{pmatrix} \\in S_1 \\Rightarrow x_2 y_2 \\ge 0$.<br>\n",
    "Consider $\\begin{pmatrix} x_1+x_2 \\\\ y_1+y_2 \\end{pmatrix}$. Checking the membership criterion yields the expression\n",
    "$(x_1+x_2)(y_1+y_2) = x_1 y_1 + x_2 y_2 + x_1 y_2 + y_2 x_1$.<br> The first two terms are non-negative, but the cross terms could be either."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Can we construct a counterexample?<br>\n",
    "$\\quad\\;\\ $We could construct one simply by looking at the righthand side of this expression.<br>\n",
    "$\\quad\\;\\ $This is made even simpler by looing at the figure:<br>\n",
    "$\\qquad\\qquad$look at the vectors $v_1, v_2$, and notice that their sum is in quadrant 4, not in $S_1$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\quad\\;\\ $$S_1$ is not closed under addition:<br>\n",
    "$\\quad\\;\\ $Let $v_1 = \\begin{pmatrix} 2\\\\1 \\end{pmatrix}$ and $v_2 = \\begin{pmatrix} -1\\\\-2 \\end{pmatrix}$ for example. Both of these vectors satisfy the membership constraint,<br>\n",
    "$\\quad\\;\\ $and are therefore in $S_1$.\n",
    "\n",
    "$\\quad\\;\\ $ $v_1 + v_2 = \\begin{pmatrix}1\\\\-1\\end{pmatrix}$ however does not satisfy the membership constraint.<br>\n",
    "$\\quad\\;\\ $ **Since $S_1$ is not closed under addition, it is not a subspace of $\\mathbb{R}^2$.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### $S_2$ is similar."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This time, it is condition 3 (closure under scalar multiplication) that is violated.\n",
    "\n",
    "**Counterexample:**<br>\n",
    "$\\qquad$Let $u_1 = \\begin{pmatrix}0\\\\1 \\end{pmatrix}.$ It is in $S_2$ since it satisfies the membership constraint. The vector $-u_1 = \\begin{pmatrix}0\\\\ -1 \\ \\end{pmatrix}$ is not in $S_2.$<br>\n",
    "$\\qquad$**Since $S_2$ is not closed under scalar multiplication, it is not a subspace of $\\mathbb{R}^2$.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.2.1.4 Another Set of Vectors that is Not a Hyperplane"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"Figs/ellipse.svg\"Figs/\n",
    "     style=\"float: left; margin-right: 10px;\" width=250/>\n",
    "\n",
    "Consider $S = \\left\\{ \\begin{pmatrix} x\\\\y \\end{pmatrix} \\ \\bigg| \\ \\left(\\frac{x}{4}\\right)^2 + \\left(\\frac{y}{2}\\right)^2 \\le 1 \\right\\}$\n",
    "\n",
    "This is the interior and boundary of an ellipse (Reminder: intermediate value theorem.)\n",
    "\n",
    "Note that Property 3. does not hold:<br>\n",
    "$\\quad$ Let $v = \\begin{pmatrix} 2\\\\0\\end{pmatrix}$. The vector $v$ is in $S$ since $\\left(\\frac{2}{4}\\right)^2 + 0 < 1$.<br>\n",
    "$\\quad$ The vector $5 v = \\begin{pmatrix} 10\\\\0\\end{pmatrix}$ is not in $S$since $\\left(\\frac{10}{4}\\right)^2 + 0 > 1$.\n",
    "\n",
    "Since $S$ is not closed under scalar multiplication, **$S$ is not a subspace of $\\mathbb{R}^2$.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2.2 Matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.2.2.1 Invertible Matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the set $M = \\left\\{ A \\in \\mathscr{M}_{2 2}  \\ \\bigg| \\  A^{-1} \\text{ exists} \\right\\}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. The zero vector is $Z = \\begin{pmatrix} 0 & 0 \\\\ 0 & 0 \\end{pmatrix}$.<br>\n",
    "Since $Z^{-1}$ does not exist, the zero vector is not in $M$.<br>\n",
    "**Since the zero vector is not in $M$, this set of vectors $M$ is not a subspace of $\\mathscr{M}_{2 2}$**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2.3 Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.2.3.1 The subspace $\\mathscr{C}^2(-1,1)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the space of functions $\\mathscr{C}^2(-1,1) = \\left\\{ f(x)  \\ \\bigg| \\  \\frac{d^2}{dx^2} f(x)\\quad\\text{is continuous for } -1 < x < 1 \\right\\}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. The zero vector $z(x) = 0$ is in this subset since $z''(x) = 0$ is continuous on  the interval $(-1,1)$.<br> **$\\mathscr{C}^2(-1,1)$  is not empty.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. Let $f_1(x) \\in \\mathscr{C}^2(-1,1) \\Rightarrow f_1'' \\text{ is continuous on } (-1,1)$<br>\n",
    "Let $f_2(x) \\in \\mathscr{C}^2(-1,1) \\Rightarrow f_2'' \\text{ is continuous on } (-1,1)$<br>\n",
    "Since $(f_1+f_2)'' = f_1''+f_2''$ is continuous on $ (-1,1)$,<br> **$\\mathscr{C}^2(-1,1)$ is closed under addition.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. Let $f_1(x) \\in \\mathscr{C}^2(-1,1) \\Rightarrow f_1'' \\text{ is continuous on } (-1,1)$<br>\n",
    "Let $\\alpha_1$ be any scalar in $\\mathbb{R}$.<br>\n",
    "Since $(\\alpha_1 f_1)'' = \\alpha_1 f_1''$ is continuous on $(-1,1)$,<br>\n",
    " **$\\mathscr{C}^2(-1,1)$ is closed under scalar multiplication.**\n",
    " \n",
    "\n",
    "**Since 1,2, and 3 hold, $\\mathscr{C}^2(-1,1)$ is a subspace of $\\mathscr{F}(-1,1)$.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 2.2.3.2 A Set of Functions that is Not a Subspace"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider $S = \\left\\{ f(x)\\ \\bigg| \\ f(x) = \\alpha + 2 x + \\beta x^2 \\quad \\text{for all }\\alpha, \\beta \\text{ in } \\mathbb{R}, -\\infty < x < \\infty\\right\\}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The vectors are functions $f(x)$ defined on the domain $-\\infty < x < \\infty$.\n",
    "\n",
    "1. The zero vector $z(x) = 0$. To check whether it is in $S$, we need to find constants $\\alpha$ and $\\beta$ such that $z(x) = \\alpha + 2x + \\beta x^2$ for all $-\\infty < x < \\infty$.<p><br>\n",
    "    $$\\begin{align}\n",
    "    (\\xi) \\Leftrightarrow &\\ \\alpha + 2x + \\beta x^2 & = 0 & \\quad\\text{for all } -\\infty < x < \\infty \\\\\n",
    "          \\Rightarrow &\\ \\alpha & = 0 & \\quad\\text{at } x=0\\\\\n",
    "    (\\xi) \\Rightarrow &\\ \\alpha + 2 + \\beta &= 0 & \\quad\\text{at } x = 1\\\\\n",
    "    (\\xi) \\Rightarrow &\\ \\alpha - 2 + \\beta &= 0 & \\quad\\text{at } x = -1\\\\\n",
    "    \\end{align}\n",
    "    $$\n",
    "    Since the three equations for $\\alpha$ and $\\beta$ result in a contradiction,\n",
    "    the zero vector is not in $S$.<br>\n",
    "    **$S$ is not a subspace of $\\mathscr{F}(-\\infty,\\infty)$**.\n",
    "    </p>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Take Away"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Checking whether a set of vectors $S$ in a vector space $V$ forms a subspace of $V$:\n",
    "> * try to write it as a span\n",
    "> * check whether the zero vector is in $S$.\n",
    "> * check whether there is an easy counterexample\n",
    "> * write out a proof using the template provided above"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.3",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
