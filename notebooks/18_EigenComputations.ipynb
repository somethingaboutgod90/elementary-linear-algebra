{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align: center;\"><strong style=\"height:100px;color:darkred;font-size:40px;\">Eigenproblem Computations</strong></div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "html\"<iframe width=\\\"400\\\" height=\\\"200\\\" src=\\\"https://www.youtube.com/embed/DbbofHl3MaI\\\"  frameborder=\\\"0\\\" allow=\\\"accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture\\\" allowfullscreen></iframe>\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Stochastic Matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "Matrices that arise in probability theory have rows or columns (or both) that add to 1.\n",
    "\n",
    "\n",
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:**\n",
    "*    A **right stochastic matrix** is a real square matrix with non-negative entries, with each row summing to 1.\n",
    "*    A **left stochastic matrix** is a real square matrix with non-negative entries,  with each column summing to 1.\n",
    "</div>\n",
    "\n",
    "Stochastic matrices are a **special case** of a more general class of matrices<br>\n",
    "with either rows and/or columns summing to the same constant value."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "**Remark:**\n",
    "* since $det(A) = det(A^t)$, **a square matrix $A$ and its transpose $A^t$ have the same eigenvalues.**\n",
    "* Let $\\mathbf{\\mathscr{1}}$ be a vector of all entries equal to 1\n",
    "    * $A \\mathbf{\\mathscr{1}}$ yields a vector with row entries equal to the sum of the corresponding row of $A.$<br>\n",
    "      If **each row of $A$ sums to the same value** $\\lambda$, then $\\lambda$ is an eigenvalue of $A$ with eigenvector $\\mathbf{\\mathscr{1}}$\n",
    "    * $A^t \\mathbf{\\mathscr{1}}$ yields a vector with row entries equal to the sum of the corresponding col of $A.$<br>\n",
    "    If **each column of $A$ sums to the same value** $\\lambda$, then $\\lambda$ is an eigenvalue of $A$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "##### **Check for an Eigenvalue**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "Given a square matrix $A$, we can check for an easy eigenvalue by **summing each row and each column**.\n",
    "    \n",
    "**Example:**<br>\n",
    "    Let $A = \\begin{pmatrix} 1&3&1\\\\ 2&4& -1 \\\\ -2&5&2 \\end{pmatrix}, \\quad\n",
    "    B = \\begin{pmatrix} 3&3&1\\\\ 6&4& -1 \\\\ -4&-2&5 \\end{pmatrix}$\n",
    "    \n",
    "The rows of $A$ sum to 5, so **5 is an eigenvalue** of $A$, and $\\mathbb{1}$ is a corresponding eigenvector.<br>\n",
    "The cols of $B$ sum to 5, so **5 is an eigenvalue** of $A$. (Note $\\mathbb{1}$ is the corresponding eigenvector of $A^t$.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Null Space Computations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "##### **The null space for a matrix with a single non-zero row**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "Let $A = \\begin{pmatrix} a_1 & a_2 & a_3 & \\dots & a_n \\\\\n",
    "                          0  &  0  &  0  & \\dots & 0 \\\\\n",
    "                          \\dots  &  \\dots  & \\dots & \\dots & \\dots \\end{pmatrix}$ be a matrix in row echelon form."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "To construct a basis for the null-space, observe that\n",
    "$$\n",
    "    \\begin{pmatrix} a_1 \\\\ \\dots \\\\ a_i \\\\ \\dots \\\\ a_j \\\\ \\dots \\\\ a_n\\end{pmatrix} \\cdot\n",
    "    \\begin{pmatrix} 0   \\\\ \\dots \\\\ -a_j \\\\ \\dots \\\\ a_i \\\\ \\dots \\\\ 0\\end{pmatrix}  = 0,\\quad\\quad\n",
    "    \\text{where the second vector has zero entries except for } a_i, a_j \\ \\text{for some } i \\ne j.\n",
    "$$\n",
    "\n",
    "Let $v_{i j}$ denote this second vector.<br>Require one of $a_i, a_j$ to be non-zero to ensure this is not a zero vector."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "**Method:**\n",
    "* pick a non-zero entry in $A$, say $a_i$ in $A$.\n",
    "$$ \\text{Basis}\\ \\mathscr{N}(A) = \\{ v \\ \\mid \\ v = v_{i j},\\ i \\ne j,\\ j=1,2, \\dots n \\}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "##### **Example:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "true"
   },
   "source": [
    "Obtain a basis for the null space $\\mathscr{N}(A)$, where\n",
    "$A =  \\begin{pmatrix} 0 & \\color{red}3 & 2 & 0 & 4 \\\\\n",
    "                      0 & 0 & 0 & 0 & 0 \\\\ \\end{pmatrix}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "Choose $a_2 = 3$ for the non-zero entry.\n",
    "\n",
    "A basis for $\\mathscr{N}(A) = \\left\\{\\;\n",
    "       \\begin{pmatrix} \\color{red}{-3}   \\\\ 0 \\\\ 0 \\\\ 0 \\\\ 0 \\end{pmatrix},\n",
    "       \\begin{pmatrix} 0    \\\\ -2 \\\\ \\color{red}3 \\\\ 0 \\\\ 0 \\end{pmatrix},\n",
    "       \\begin{pmatrix} 0    \\\\ 0 \\\\ 0 \\\\ \\color{red}3 \\\\ 0 \\end{pmatrix},\n",
    "       \\begin{pmatrix} 0    \\\\ -4 \\\\ 0 \\\\ 0 \\\\ \\color{red}3 \\end{pmatrix}\n",
    "\\; \\right\\}$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "This construction yields the desired number of linearly independent vectors, and hence a basis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Matrices of Size $2 \\times 2$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "#### **Theory**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "Let $A = \\begin{pmatrix} a & b \\\\ c & d \\end{pmatrix}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "##### **The characteristic polynomial**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "$$\n",
    "\\begin{align}\n",
    "p(\\lambda) & = (\\lambda - \\lambda_1) ( \\lambda - \\lambda_2 ) \\\\\n",
    "           & = \\lambda^2 - (\\lambda_1 + \\lambda_2) \\lambda + \\lambda_1 \\lambda_2 \\\\\n",
    "           & = \\lambda^2 - trace (A)\\ \\lambda + det(A) .\n",
    "\\end{align}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "##### **The eigenvector basis**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "Given a $2 \\times 2$ non-zero matrix $A$\n",
    "\n",
    "Let $A$ have eigenvalues $\\lambda_1, \\lambda_2$. Pick one of the eigenvalues, say $\\lambda_1$.\n",
    "\n",
    "The Cayley-Hamilton theorem can be used to show that<br>\n",
    "$\\quad\\quad$ **a non-zero column of $A -{\\lambda_1} I$\n",
    "is an eigenvector for $\\lambda_2$.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "<span style=\"color:red;\"><strong>Caveat:</strong></span> this applies **only** to matrices of size $2 \\times 2.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "#### **Example:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "Let $A = \\begin{pmatrix} 4 & 1 \\\\ 1 & 4 \\end{pmatrix}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "**Step 1:** Since $trace(A) = 8, $ and $det A = 15$, we have $\\quad \\mathbf{p(\\lambda) = \\lambda^2 - 8 \\lambda + 15}.$<br>\n",
    "$\\quad\\quad\\therefore\\;$ The roots are $\\lambda = 3,5$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "**Step 2:** Pick one of the eigenvalues, say $\\lambda = 3$\n",
    "$$\n",
    "A - 3 I = \\begin{pmatrix} \\color{red}1 & 1 \\\\ \\color{red}1 & 1 \\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "* $\\begin{pmatrix} 1 \\\\ 1 \\end{pmatrix}$ is an eigenvector for $\\lambda=5$ (a non-zero row column of $A-3I$)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "* $\\begin{pmatrix}-1 \\\\ 1 \\end{pmatrix}$ is an eigenvector for $\\lambda=3$ (null space for a single non-zero row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "**Remark:** We know $(A - 3I)$ reduces to row echelon form $\\begin{pmatrix} 1 & 1 \\\\ 0 & 0 \\end{pmatrix}$\n",
    "since there must be a missing pivot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. Matrices of Size $3 \\times 3$ with a Known Non-zero Eigenvalue"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "We can exploit a known non-zero eigenvalue of $A$ and the trace and determinants of $A$<br>\n",
    "to speed up computations:\n",
    "\n",
    "Consider the root form of the characteristic polynomial\n",
    "$$\n",
    "\\begin{align}\n",
    "det( A - \\lambda I) & = - (\\lambda - \\lambda_1)(\\lambda - \\lambda_2)(\\lambda - \\lambda_3) \\\\\n",
    "    &=  - (\\lambda - \\lambda_1) \\left( \\lambda^2 - ( \\lambda_2+\\lambda_3 ) \\lambda + \\lambda_2 \\lambda_3 \\right)\\label{eq1}\\tag{1}\n",
    "\\end{align}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "##### **Example:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "$$\n",
    "A = \\begin{pmatrix}\n",
    "1 & 1 & 1 \\\\\n",
    "0 & -1 & 3 \\\\\n",
    "    1 & 2 & -2 \\end{pmatrix}.\n",
    "$$\n",
    "\n",
    "Since the columns add to 2, we know an eigenvalue $\\lambda_1 = 2$ of $A$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "The $trace \\ A = -2,$ and $ det \\ A = 0$.\n",
    "\n",
    "Therefore, using Eq 1,\n",
    "$$\n",
    "\\left.\n",
    "\\begin{align}\n",
    "\\lambda_1 & = 2  &\\\\\n",
    "\\lambda_2 + \\lambda_3 &= trace ( A ) -  \\lambda_1 =& -4 \\\\\n",
    "\\lambda_2 \\lambda_3   &= \\frac{1}{\\lambda_1} det (A) =& 0\n",
    "\\end{align} \\right\\} \\Rightarrow \\; p(\\lambda) = -(\\lambda - 2) ( \\lambda^2 + 4 \\lambda + 0 ).\n",
    "$$\n",
    "\n",
    "The roots of the quadratic are $\\lambda_2 = -4, \\lambda_3 = 0$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "**Remark:** actually, the previous example simplifies even more:<br>\n",
    "$\\quad\\quad$ We **know two eigenvalues:** $det(A) = 0 \\Leftrightarrow \\lambda_2 = 0$ is an eigenvalue.<br>\n",
    "$\\quad\\quad$ Using the trace of $A$, we have $\\lambda_3 = trace (A) - \\lambda_1 - \\lambda_2 = -4$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5. Determinants that Can be Factored"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "The following theorem can prove useful\n",
    "\n",
    "Let $A$ be a square matrix with a partitioning of the form\n",
    "$$\n",
    "A = \\begin{pmatrix} A_1 & B \\\\ C & A_2 \\end{pmatrix},\n",
    "$$\n",
    "\n",
    "such that $A_1$ and $A_2$ are square, and either $B = 0$ or $C = 0$.<br>\n",
    "Then $det A = ( det A_1 ) (det A_2 )$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "**Example:**\n",
    "$$ A = \\left( \\begin{array}{cc|cc} \\color{red} 1 & \\color{red}2 & \\color{blue}0 & \\color{blue}0 \\\\\n",
    "                       \\color{red}2 & \\color{red}3 & \\color{blue}0 & \\color{blue}0 \\\\ \\hline\n",
    "                       8 & 3 & \\color{red}2 & \\color{red}1 \\\\\n",
    "    7 & 8 & \\color{red}0 & \\color{red}2 \\end{array} \\right) \\Rightarrow\n",
    "det( A - \\lambda I) = det(A_1 - \\lambda I) det(A_2 - \\lambda_I)\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\text{where } \\quad A_1 = \\begin{pmatrix} 1 & 2 \\\\ 2 & 3 \\end{pmatrix}, \\quad A_2 = \\begin{pmatrix} 2 & 1 \\\\ 0 & 2 \\end{pmatrix}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "I encourage you to solve the eigenproblems for $A_1$ and $A_2$, and answer the following questions:\n",
    "* can you construct eigenvectors for $A$ from the eigenvectors of $A_1$, $A_2$?\n",
    "* is $A$ diagonalizable?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.3",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
